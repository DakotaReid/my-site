<?
require_once(dirname(__FILE__) . "/base.class.php");

class Login extends Base_Class {
	private $AESKeyStr;
	
	public function __construct()
	{
		parent::__construct("login", "user_id");
		
		$this->AESKeyStr = "hwrpZs6VwonpY4fH";
	}
	
	public function save()
	{
		$saved = true;
		$saveValues = "";
		$sql = "";
		$values = [];
		
		$count = 0;
		foreach ($this->data as $name => $value)
		{
//			//http://www.w3resource.com/mysql/encryption-and-compression-functions/aes_encrypt().php
//			//http://stackoverflow.com/questions/18534091/php-pdo-insert-query
//			if (strpos($name, "password") !== false)
//			{
//				$saveValues .= ($count++ > 0 ? ", " : "") . $name . " = AES_ENCRYPT(?, ?)";
//				
//				$values[] = $value;
//				$values[] = $this->AESKeyStr;
//			} else {
//				$saveValues .= ($count++ > 0 ? ", " : "") . $name . " = ?";
//				
//				$values[] = $value;
//			}
			
			$saveValues .= ($count++ > 0 ? ", " : "") . $name . " = ?";
			$values[] = $value;
		}
		
		if (isset($this->data[$this->keyField]) && !empty($this->data[$this->keyField]))
		{
			$sql = "UPDATE " . $this->tableName . " SET ";
			$sql .= $saveValues;
			$sql .= " WHERE " . $this->keyField . " = ?";

			$values[] = $this->data[$this->keyField];
		} else {
			$sql = "INSERT INTO " . $this->tableName . " SET ";
			$sql .= $saveValues;
		}
		
		$stmt = $this->db->prepare($sql);
		
		if (!$stmt->execute($values))
		{
			$saved = false;
			$this->errors[] = $stmt->errorInfo();
		}
		
		return $saved;
	}
	
	public function checkCredentials()
	{		
		$stmt = $this->db->prepare("SELECT * FROM " . $this->tableName . " WHERE username = ? AND password = ?");
		
		$stmt->execute(array_values($this->data));
		
		if ($stmt->rowCount() >= 1)
		{
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
			{
				$_SESSION["credentials"] = [
					"user_id"		=> $row["user_id"],
					"username"		=> $row["username"],
					"user_level"	=> $row["user_level"]
				];
			}
			
			return true;
		}
		
		$this->errors[] = "Username or Password is incorrect";
		return false;
	}
}
?>