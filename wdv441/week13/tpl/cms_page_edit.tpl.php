<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Week 13 Homework - CMS Page Edit</title>
	
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<script>tinymce.init({ selector:'textarea' });</script>
</head>

<body>
	<h2>Edit Page</h2>
	
	<? if (!empty($cms_page->errors)) { ?>
		<div class="errors">
			<p>Error(s):</p>
			<ul>
				<? foreach ($cms_page->errors as $field_key => $error_message) { ?>
					<ul><label for="<?= $field_key; ?>"><?= $error_message; ?></label></ul>
				<? } ?>
			</ul>
		</div>
	<? } ?>
	
	<form method="POST" action="cms_page_edit.php" enctype="multipart/form-data">
        <? if (isset($values["page_id"])) { ?>
			<input type="hidden" name="page_id" value="<?= $values["page_id"]; ?>" />
		<? } ?>
		
		<div>
			<label for="page_title">Page Title: </label><br />
			<input type="text" name="page_title" id="page_title" value="<?= (isset($values["page_title"])) ? $values["page_title"] : ""; ?>" />
		</div>
		
		<br />
		
		<div>
			<label for="h1">H1: </label><br />
			<input type="text" name="h1" id="h1" value="<?= (isset($values["h1"])) ? $values["h1"] : ""; ?>" />
		</div>
		
		<br />
		
		<div>
			<label for="banner_img">Banner Image: </label><br />
			<input type="file" name="cms_pages_image" id="banner_img" />
		</div>
		
		<br />
		
		<div>
			<label for="meta_tags">Meta Tags: </label><br />
			<input type="text" name="meta_tags" id="meta_tags" value="<?= (isset($values["meta_tags"])) ? $values["meta_tags"] : ""; ?>" />
		</div>
		
		<br />
		
		<div>
			<label for="url_key">URL Key: </label><br />
			<input type="text" name="url_key" id="url_key" value="<?= (isset($values["url_key"])) ? $values["url_key"] : ""; ?>" />
		</div>
		
		<br />
		
		<div>
			<label for="content">Content: </label><br />
			<textarea name="content" id="content"><?= (isset($values["content"])) ? $values["content"] : ""; ?></textarea>
		</div>
		
		<br />
		
		<input type="submit" value="Submit" />
		<a href="index.php">Cancel</a>
    </form>
</body>
</html>