<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Week 6 Homework - Article List</title>
</head>

<body>
	<h2>Articles</h2>
	
	<? if (isset($_GET["saved_article"]) && $_GET["saved_article"]) { ?>	
		<div>
			<p>Article has been saved.</p>
		</div>
	<? } ?>
	
	<a href="article_edit.php">Create new Article</a>
	
	<br /><br />
	
	<form method="GET" action="index.php">
		<input type="text" name="searchVal" id="searchVal" />
		<input type="submit" value="Submit" />
		
		<? if (isset($_GET["searchVal"])) { ?>
			<a href="index.php" style="color: black; text-decoration: none;">X</a>
		<? } ?>
	</form>
	
	<br />
	
	<table>
		<tr>
			<? foreach ($article->colsToDisplay as $header => $col) { ?>
				<th><?= $header; ?></th>
			<? } ?>
			<th colspan="2"></th>
		</tr>
		
		<? while ($row = $list->fetch(PDO::FETCH_ASSOC)) { ?>
			<tr>
				<? foreach ($article->colsToDisplay as $header => $col) { ?>
					<? if ($col != "article_date") { ?>
						<td><?= $row[$col]; ?></td>
					<? } else { ?>
						<td><?= date("Y-m-d h:i:s A", strtotime($row[$col])); ?></td>
					<? } ?>
				<? } ?>
				
				<td><a href="article_view.php?article_id=<?= $row["article_id"]; ?>">View</a></td>
				<td><a href="article_edit.php?article_id=<?= $row["article_id"]; ?>">Edit</a></td>
			</tr>
		<? } ?>
	</table>
</body>
</html>