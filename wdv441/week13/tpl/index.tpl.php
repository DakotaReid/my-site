<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Week 8 Homework - Index</title>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
	
	<script>
		$(document).ready(function() {
			$("#weather").load("weather_widget.php");
		});
	</script>
</head>

<body>
	<nav>
		<ul>
			<? while ($row = $cms_pages->fetch(PDO::FETCH_ASSOC)) { ?>
				<li><a href="cms_page_view.php?url_key=<?= $row["url_key"]; ?>"><?= $row["h1"]; ?></a></li>
			<? } ?>
		</ul>
	</nav>
	
	<? if (!empty($login->errors)) { ?>
		<div class="errors">
			<p>Error(s):</p>
			
			<ul>
				<? foreach ($login->errors as $field_key => $error_message) { ?>
					<ul><label for="<?= $field_key; ?>"><?= $error_message; ?></label></ul>
				<? } ?>
			</ul>
		</div>
	<? } ?>
	
	<form method="POST" action="index.php">
		<div>
			<label for="username">Username: </label><br />
			<input type="text" name="username" id="username" />
		</div>
		
		<div>
			<label for="password">Password:</label><br />
			<input type="password" name="password" id="password" />
		</div>
		
		<br />
		
		<input type="submit" value="Submit" />
		<a href="index.php">Cancel</a>
	</form>
	
	<br />
	
	<div id="weather"></div>
	
	<br />
	
	<a href="user_edit.php">Add a User</a><br />
	
	<a href="users_list.php">User List</a><br />
	
	<a href="cms_page_edit.php">Add a CMS Page</a><br />
	
	<a href="cms_page_list.php">CMS Pages</a><br />
	
	<a href="rest_get_users.php">REST Users</a>
</body>
</html>