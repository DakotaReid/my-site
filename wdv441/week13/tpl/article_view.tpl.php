<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Week 6 Homework - Article View</title>
</head>

<body>
	<h2>Article</h2>
	
	<? foreach ($article->data as $key => $value) { ?>
		<? if ($key != "article_id") { ?>
			<p><?= ucwords(str_replace("_", " ", $key)); ?>: <?= $value; ?></p>
		<? } ?>
	<? } ?>
	<br />
	
	<a href="index.php">Back</a>
</body>
</html>