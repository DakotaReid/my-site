<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Week 8 Homework - User Edit</title>
</head>

<body>
	<? if (!empty($login->errors)) { ?>
		<div class="errors">
			<p>Error(s):</p>
			
			<ul>
				<? foreach ($login->errors as $field_key => $error_message) { ?>
					<ul><label for="<?= $field_key; ?>"><?= $error_message; ?></label></ul>
				<? } ?>
			</ul>
		</div>
	<? } ?>
	
	<form method="POST" action="user_edit.php" enctype="multipart/form-data">
        <? if (isset($values["user_id"])) { ?>
			<input type="hidden" name="user_id" value="<?= $values["user_id"]; ?>" />
		<? } ?>
		
		<div>
			<label for="username">Username: </label><br />
			<input type="text" name="username" id="username" value="<?= (isset($values["username"])) ? $values["username"] : ""; ?>" autocomplete="off" />
		</div>
		
		<div>
			<label for="password">Password:</label><br />
			<input type="password" name="password" id="password" value="<?= (isset($values["password"])) ? $values["password"] : ""; ?>" autocomplete="off" />
		</div>
		
		<div>
			<label for="user_level">User Level:</label>
			<select name="user_level" id="user_level">
				<option value="1" <?= (isset($values["user_level"]) && $values["user_level"] == 1) ? "selected" : ""; ?>>User</option>
				<option value="2" <?= (isset($values["user_level"]) && $values["user_level"] == 2) ? "selected" : ""; ?>>Admin</option>
			</select>
		</div>
		
		<br />
		
		<input type="submit" value="Submit" />
		<a href="index.php">Cancel</a>
    </form>
</body>
</html>