<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Week 13 Homework - CMS Pages</title>
</head>

<body>
	<h2>CMS Pages</h2>
	
	<a href="cms_page_edit.php">Add a Page</a>
	
	<br /><br />
	
	<form method="GET" action="cms_page_list.php">
		<input type="text" name="searchVal" id="searchVal" />
		<input type="submit" value="Submit" />
		
		<? if (isset($_GET["searchVal"])) { ?>
			<a href="cms_page_list.php" style="color: black; text-decoration: none;">X</a>
		<? } ?>
	</form>
	
	<br />
	
	<table>
		<tr>
			<? foreach ($cms_page->colsToDisplay as $header => $col) { ?>
				<th><?= $header; ?></th>
			<? } ?>
			<th colspan="2"></th>
		</tr>
		
		<? while ($row = $list->fetch(PDO::FETCH_ASSOC)) { ?>
			<tr>
				<? foreach ($cms_page->colsToDisplay as $header => $col) { ?>
					<td><?= $row[$col]; ?></td>
				<? } ?>
				
				<td><a href="cms_page_view.php?url_key=<?= $row["url_key"]; ?>">View</a></td>
				<td><a href="cms_page_edit.php?page_id=<?= $row["page_id"]; ?>">Edit</a></td>
			</tr>
		<? } ?>
	</table>
	
	<div>
		<br />
		<a href="users_report.php">Download Report</a>
		
		<br />
		<br />
		<a href="index.php">Back</a>
	</div>
</body>
</html>