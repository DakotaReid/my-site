<?
require_once(dirname(__FILE__) . "/common.func.php");

class Base_Class {
	public $data;
	public $db;
	public $errors;
	public $tableName;
	public $keyField;
	public $searchableFields;
	
	public function __construct($tableName, $keyField)
	{
		$this->data = [];
		$this->errors = [];
		$this->tableName = $tableName;
		$this->keyField = $keyField;
		
		$this->connectToDB();
	}
	
	public function connectToDB() {
		$this->db = new PDO("mysql:host=localhost;dbname=wdv_441;charset=utf8", "wdv441", "wdv441");
		$connectedToDB = true;
		
		if (!$this->db)
		{
			$errors["database"] = "Unable to connect to the database";
			$connectedToDB = false;
		}
		
		return $connectedToDB;
	}
	
	public function set($data)
	{		
		$this->data = $this->sanitize($data);
		
		return true;
	}
	
	public function sanitize($data)
	{
		$data = filter_var_array($data, FILTER_SANITIZE_STRING);
		
		return $data;
	}
	
	public function validate()
	{
		$is_valid = false;
		
		if (empty($this->data)) {
			return $is_valid;
		}
		
		//$this->data["article_date"] = "2016-22-02";
		
		foreach ($this->data as $key => $value)
		{			
			if (!strpos($key, "date") === false)
			{
				//Regex isn't working for some reason...
				
				//	/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/
				
				print_r($value);
				$date = DateTime::createFromFormat("Y-m-d", $value);
				
				print_r($date);
				die;
				
				$is_valid = preg_match("/^(19|20)\d\d[-.](0[1-9]|1[012])[-.](0[1-9]|[12][0-9]|3[01])$/", $this->data[$key]);
				
				if (!$is_valid)
				{
					$this->errors[$key] = ucwords(str_replace("_", " ", $key)) . " is an invalid date";
				}
			} else {
				if (empty($value) || $value == "")
				{
					$this->errors[$key] = ucwords(str_replace("_", " ", $key)) . " is required";
				} else {
					//$value is a string so it passes
					$is_valid = true;
				}
			}
		}
		//var_dump($this->errors);
		//die;
		return $is_valid;
	}
	
	public function load($id)
	{
		$loadedData = true;
		
		$stmt = $this->db->prepare("SELECT * FROM " . $this->tableName . " WHERE " . $this->keyField . " = ?");
		
		$stmt->execute(array($id));
		
		if ($stmt->rowCount() >= 1)
		{
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
			{
				$this->data = $row;
			}
			
			$loadedData = true;
		}
		
		return $loadedData;
	}
	
	public function save()
	{
		$saved = true;
		$saveValues = "";
		$sql = "";
		$values = [];
		
		$count = 0;
		foreach ($this->data as $name => $value)
		{
			$saveValues .= ($count++ > 0 ? ", " : "") . $name . " = ?";
		}
		
		if (isset($this->data[$this->keyField]) && !empty($this->data[$this->keyField]))
		{
			$sql = "UPDATE " . $this->tableName . " SET ";
			$sql .= $saveValues;
			$sql .= " WHERE " . $this->keyField . " = ?";
			
			$values = array_values($this->data);
			$values[] = $this->data[$this->keyField];
		} else {
			$sql = "INSERT INTO " . $this->tableName . " SET ";
			$sql .= $saveValues;
			
			$values = array_values($this->data);
		}
			
		$stmt = $this->db->prepare($sql);
		
		if (!$stmt->execute($values))
		{
			$saved = false;
			$this->errors[] = $stmt->errorInfo();
		} else {
			$this->data[$this->keyField] = $this->db->lastInsertId();
		}
		
		return $saved;
	}
	
	public function getList($search = null)
	{
		$sql = "SELECT * FROM " . $this->tableName;
		$params = [];
		
		if (!is_null($search) && !empty($search))
		{
			$sql .= " WHERE ";
			
			$fieldCount = 0;
			foreach ($this->searchableFields as $field)
			{
				$sql .= ($fieldCount++ > 0 ? " OR " : "") . $field . " LIKE ?";
				$params[] = "%" . $search . "%";
			}
		}
		
		$stmt = $this->db->prepare($sql);
		$stmt->execute($params);
		
		return $stmt;
	}
	
	public function getListByID($id)
	{
		$sql = "SELECT * FROM " . $this->tableName . " WHERE " . $this->keyField . "= ?";
		$params[] = $id;
		
		$stmt = $this->db->prepare($sql);
		$stmt->execute($params);
		
		return $stmt;
	}
	
	public function saveUploadedImage($files)
	{
		if (!empty($files) && !empty($files[$this->tableName . "_image"]["name"]))
		{
			$imagesDir = dirname(__DIR__) . "/public_html/images/";
			$filePathInfo = pathinfo($files[$this->tableName . "_image"]["name"]);
			$targetFile = $imagesDir . $this->data[$this->keyField] . "_" . $this->tableName . "." . $filePathInfo["extension"];
			$validImageExtensions = ["gif", "jpg", "jpeg", "png"];
			
			//var_dump($files[$this->tableName . "_image"]["name"]);
			//var_dump($targetFile);
			
			if (in_array($filePathInfo["extension"], $validImageExtensions))
			{
				if (!move_uploaded_file($files[$this->tableName . "_image"]["tmp_name"], $targetFile))
				{
					$this->errors[] = "Unable to Upload Your File";
				}
			}
		}
	}
	
	public function generateReport($reportFilters, $download = false)
	{		
		$sql = "SELECT * FROM " . $this->tableName;
		
		if (!is_null($reportFilters) && !empty($reportFilters))
		{
			$sql .= " WHERE ";
			
			$fieldCount = 0;
			foreach ($reportFilters as $key => $field)
			{
				$sql .= ($fieldCount++ > 0 ? " AND " : "") . $key . " LIKE ?";
				$params[] = "%" . $field . "%";
			}
			
			$stmt = $this->db->prepare($sql);
			$stmt->execute($params);
		} else {
			$stmt = $this->db->query($sql);
		}
		
		if ($download)
		{
			header("Content-Type: text/csv");
			header('Content-Disposition: attachment; filename="users_report_' . date("Y-m-d_h-m-s") . '.csv"');
			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
			{
				echo implode(",", $row) . "\r\n";
			}
		}
		
		return $stmt;
	}
	
	public function rest_get($id = null)
	{
		$rest_output = [];
		
		$list = [];
		if (!empty($id))
		{
			$list = $this->getListByID($id);
		} else {
			$list = $this->getList();
		}
		
		while ($row = $list->fetch(PDO::FETCH_ASSOC))
		{
			$rest_output[] = $row;
		}
		
		return json_encode($rest_output);
	}
	
	static function curlGet($url)
	{
		$session = curl_init();
		
		$result = false;
		if ($session)
		{
			curl_setopt_array($session, [
				CURLOPT_URL 			=> $url,
				CURLOPT_FOLLOWLOCATION	=> true,
				CURLOPT_RETURNTRANSFER	=> true
			]);
			
			$result = curl_exec($session);
		}
		curl_close($session);
		
		return $result;
	}
}

session_start();
?>