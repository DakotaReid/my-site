<?
require_once(dirname(__FILE__) . "/base.class.php");

class CMS_Pages extends Base_Class {
	public function __construct()
	{
		parent::__construct($tableName = "cms_pages", $keyField = "page_id");
		
		$this->colsToDisplay = [
			"Page Title"	=> "page_title",
			"H1"			=> "h1",
			"URL Key"		=> "url_key",
			"Content"		=> "content"
		];
	}
	
	public function loadByURLKey($urlKey)
	{
		$loadedData = true;
		
		$stmt = $this->db->prepare("SELECT * FROM " . $this->tableName . " WHERE url_key = ?");
		
		$stmt->execute(array($urlKey));
		
		if ($stmt->rowCount() >= 1)
		{
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
			{
				$this->data = $row;
			}
			
			$loadedData = true;
		}
		
		return $loadedData;
	}
}
?>