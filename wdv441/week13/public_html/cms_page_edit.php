<?
require_once(dirname(__DIR__) . "/inc/cms.class.php");

$cms_page = new CMS_Pages();

if (isset($_REQUEST["page_id"]) && $_REQUEST["page_id"] > 0)
{
	$cms_page->load($_REQUEST["page_id"]);
}

if (!empty($_POST))
{
	if ($cms_page->set($_POST))
	{
		if ($cms_page->validate())
		{
			if ($cms_page->save())
			{
				$cms_page->saveUploadedImage($_FILES);
				
				redirect("index.php?saved_page=true");
			}
		}
	}
}

$values = $cms_page->data;

var_dump($cms_page->errors);

include_once(dirname(__DIR__) . "/tpl/cms_page_edit.tpl.php");
?>