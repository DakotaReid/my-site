<?
require_once(dirname(__DIR__) . "/inc/login.class.php");

$searchVal = null;

$login = new Login();

if (!empty($_GET["searchVal"]) && $_GET["searchVal"] != "")
{
	$searchVal = filter_var($_GET["searchVal"], FILTER_SANITIZE_STRING);
}

$list = $login->getList($searchVal);

include_once(dirname(__DIR__) . "/tpl/users_list.tpl.php");
?>