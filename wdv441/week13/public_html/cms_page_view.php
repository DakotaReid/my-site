<?
require_once(dirname(__DIR__) . "/inc/cms.class.php");

$cms_page = new CMS_Pages();

if (isset($_GET["page_id"]))
{
	$cms_page->load($_GET["page_id"]);
} else if (isset($_GET["url_key"])) {
	$cms_page->loadByURLKey($_GET["url_key"]);
}

include_once(dirname(__DIR__) . "/tpl/cms_page_view.tpl.php");
?>