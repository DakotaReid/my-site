<?
require_once(dirname(__DIR__) . "/inc/login.class.php");
require_once(dirname(__DIR__) . "/inc/cms.class.php");

$login = new Login();
$cms_page = new CMS_Pages();

if (!empty($_POST))
{
	if ($login->set($_POST))
	{
		if ($login->validate())
		{
			if ($login->checkCredentials())
			{
				print_r($_SESSION);
			}
		}
	}
}

$cms_pages = $cms_page->getList();

include_once(dirname(__DIR__) . "/tpl/index.tpl.php");
?>