<?
require_once(dirname(__DIR__) . "/inc/cms.class.php");

$searchVal = null;

$cms_page = new CMS_Pages();

if (!empty($_GET["searchVal"]) && $_GET["searchVal"] != "")
{
	$searchVal = filter_var($_GET["searchVal"], FILTER_SANITIZE_STRING);
}

$list = $cms_page->getList($searchVal);

include_once(dirname(__DIR__) . "/tpl/cms_page_list.tpl.php");
?>