<?
require_once(dirname(__FILE__) . "/base.class.php");

class News_Article extends Base_Class {
	public function __construct() {
		parent::__construct("news_articles", "article_id");
	}
	
	public function sanitize($data)
	{        
        return parent::sanitize($data);
	}
	
	public function validate()
	{
		return parent::validate();
	}
}
?>