<?
require_once(dirname(__DIR__) . "/inc/login.class.php");

if (empty($_SESSION["credentials"]) || $_SESSION["credentials"]["user_level"] != 2)
{
	redirect("users_list.php");
}

$login = new Login();

if (isset($_GET["download"]) && $_GET["download"])
{
	unset($_GET["download"]);
	
	$list = $login->generateReport($_GET, true);
	exit;
} else {
	$list = $login->generateReport($_GET);
}

$downloadLink = "users_report.php?download=true";
foreach ($_GET as $key => $value)
{
	$downloadLink .= "&" . $key . "=" . $value;
}


include_once(dirname(__DIR__) . "/tpl/users_report.tpl.php");
?>