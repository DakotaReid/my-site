<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Week 8 Homework - Users List</title>
</head>

<body>
	<h2>Users</h2>
	
	<a href="user_edit.php">Add a User</a>
	
	<br /><br />
	
	<form method="GET" action="users_list.php">
		<input type="text" name="searchVal" id="searchVal" />
		<input type="submit" value="Submit" />
		
		<? if (isset($_GET["searchVal"])) { ?>
			<a href="users_list.php" style="color: black; text-decoration: none;">X</a>
		<? } ?>
	</form>
	
	<br />
	
	<table>
		<tr>
			<? foreach ($login->colsToDisplay as $header => $col) { ?>
				<th><?= $header; ?></th>
			<? } ?>
			<th colspan="2"></th>
		</tr>
		
		<? while ($row = $list->fetch(PDO::FETCH_ASSOC)) { ?>
			<tr>
				<? foreach ($login->colsToDisplay as $header => $col) { ?>
					<td><?= $row[$col]; ?></td>
				<? } ?>
				
				<td><a href="user_view.php?user_id=<?= $row["user_id"]; ?>">View</a></td>
				<td><a href="user_edit.php?user_id=<?= $row["user_id"]; ?>">Edit</a></td>
			</tr>
		<? } ?>
	</table>
	
	<div>
		<br />
		<a href="users_report.php">Download Report</a>
		
		<br />
		<br />
		<a href="index.php">Back</a>
	</div>
</body>
</html>