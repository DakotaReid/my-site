<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Week 8 Homework - User View</title>
</head>

<body>
	<h2>User</h2>
	
	<? foreach ($login->data as $key => $value) { ?>
		<? if ($key != "user_id") { ?>
			<p><?= ucwords(str_replace("_", " ", $key)); ?>: <?= $value; ?></p>
		<? } ?>
	<? } ?>
	<br />
	
	<a href="users_list.php">Back</a>
</body>
</html>