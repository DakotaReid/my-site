<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Week 8 Homework - Index</title>
</head>

<body>
	<? if (!empty($login->errors)) { ?>
		<div class="errors">
			<p>Error(s):</p>
			
			<ul>
				<? foreach ($login->errors as $field_key => $error_message) { ?>
					<ul><label for="<?= $field_key; ?>"><?= $error_message; ?></label></ul>
				<? } ?>
			</ul>
		</div>
	<? } ?>
	
	<form method="POST" action="index.php">
		<div>
			<label for="username">Username: </label><br />
			<input type="text" name="username" id="username" />
		</div>
		
		<div>
			<label for="password">Password:</label><br />
			<input type="password" name="password" id="password" />
		</div>
		
		<br />
		
		<input type="submit" value="Submit" />
		<a href="index.php">Cancel</a>
	</form>
	
	<br />
	
	<a href="user_edit.php">Add a User</a>
	
	<br />
	
	<a href="users_list.php">User List</a>
</body>
</html>