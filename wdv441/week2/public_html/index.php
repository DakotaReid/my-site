<?
require_once dirname(__FILE__) . "/../inc/contact.class.php";

$contact = new Contact();
$contact_save = null;

if (!empty($_POST)) {
    $contact_save = $contact->save($_POST);
    
    if ($contact_save !== true) {
        $error_output = $contact_save;
    }
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Week 2 Homework</title>
</head>

<body>
    <? if ($contact_save !== true) { ?>
	<form method="post">
        <? if (isset($error_output)) { ?>
            <div class="errors">
                <p>Error(s):</p>
                <ul>
                    <? foreach ($error_output as $field => $error_msg) { ?>
                        <li><label for="<?= $field; ?>"><?= $error_msg; ?></label></li>
                    <? } ?>
                </ul>
            </div>
        <? } ?>
        
        <label for="first_name">First Name:</label><br />
        <input type="text" name="first_name" id="first_name" value="<?= isset($_POST["first_name"]) ? $_POST["first_name"] : ""; ?>" />

        <br /><br />

        <label for="last_name">Last Name:</label><br />
        <input type="text" name="last_name" id="last_name" value="<?= isset($_POST["last_name"]) ? $_POST["last_name"] : ""; ?>" />

        <br /><br />

        <label for="date_of_birth">Date of Birth:</label><br />
        <input type="date" name="date_of_birth" id="date_of_birth" value="<?= isset($_POST["date_of_birth"]) ? $_POST["date_of_birth"] : ""; ?>" max="<?= date("Y-m-d"); ?>" />

        <br /><br />

        <label for="email">Email:</label><br />
        <input type="email" name="email" id="email" value="<?= isset($_POST["email"]) ? $_POST["email"] : ""; ?>" />

        <br /><br />

        <label for="message">Message:</label><br />
        <textarea name="message" id="message"><?= isset($_POST["message"]) ? $_POST["message"] : ""; ?></textarea>
        
        <br /><br />
        
        <input type="submit" value="Contact Me!" />
    </form>
    <? } else { ?>
    <div>
        <p>First Name: <?= $_POST["first_name"]; ?></p>
        <p>Last Name: <?= $_POST["last_name"]; ?></p>
        <p>Date of Birth: <?= date("m/d/Y", strtotime($_POST["date_of_birth"])); ?></p>
        <p>Email: <?= $_POST["email"]; ?></p>
        <p>
            Message: <br />
            <?= $_POST["message"]; ?>
        </p>
    </div>
    <? } ?>
</body>
</html>
