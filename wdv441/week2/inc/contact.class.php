<?
require_once dirname(__FILE__) . "/base.class.php";

class Contact extends Base_Class {
    public function __construct() {
        parent::__construct();
    }

    public function validate($data, $data_type = "text") {
        return parent::validate($data, $data_type);
    }

    public function load($id) {
        return parent::load($id);
    }

    public function save($data) {
        $error_output = [];
        
        if (!$this->validate($_POST["first_name"])) {
            $error_output["first_name"] = "First Name is Invalid";
        }
        
        if (!$this->validate($_POST["last_name"])) {
            $error_output["last_name"] = "Last Name is Invalid";
        }
        
        if (!$this->validate($_POST["date_of_birth"], "date")) {
            $error_output["date_of_birth"] = "Date of Birth is Invalid";
        }
        
        if (!$this->validate($_POST["email"], "email")) {
            $error_output["email"] = "Email is Invalid";
        }
        
        if (!$this->validate($_POST["message"])) {
            $error_output["message"] = "Message is Invalid";
        }
        
        if (!empty($error_output)) {
            return $error_output;
        } else {
            $to = "contactme@dakotareid.info";
            $subject = $data["email"] . " has sent you a message";
            $message = $data["message"];
            
            return mail($to, $subject, $message);
        }
    }
}
?>