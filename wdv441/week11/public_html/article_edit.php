<?
require_once(dirname(__DIR__) . "/inc/news.class.php");

$article = new News_Article();

if (isset($_REQUEST["article_id"]) && $_REQUEST["article_id"] > 0)
{
	$article->load($_REQUEST["article_id"]);
}

if (!empty($_POST))
{
	if ($article->set($_POST))
	{
		if ($article->validate())
		{
			if ($article->save())
			{
				redirect("index.php?saved_article=true");
			}
		}
	}
}

$values = $article->data;

include_once(dirname(__DIR__) . "/tpl/article_edit.tpl.php");
?>