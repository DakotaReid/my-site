<?
require_once(dirname(__DIR__) . "/inc/login.class.php");

$login = new Login();

if (!empty($_POST))
{
	if ($login->set($_POST))
	{
		if ($login->validate())
		{
			if ($login->checkCredentials())
			{
				print_r($_SESSION);
			}
		}
	}
}

include_once(dirname(__DIR__) . "/tpl/index.tpl.php");
?>