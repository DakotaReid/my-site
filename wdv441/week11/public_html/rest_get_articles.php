<?
require_once(dirname(__DIR__) . "/inc/news.class.php");

$article = new News_Article();

$articles;
if (!empty($_GET["article_id"]))
{
	$articles = $article->rest_get($_GET["article_id"]);
} else {
	$articles = $article->rest_get();
}

echo $articles;
?>