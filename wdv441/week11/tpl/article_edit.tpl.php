<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Week 6 Homework - Article Edit</title>
</head>

<body>
	<? if (!empty($article->errors)) { ?>
		<div class="errors">
			<p>Error(s):</p>
			<ul>
				<? foreach ($article->errors as $field_key => $error_message) { ?>
					<ul><label for="<?= $field_key; ?>"><?= $error_message; ?></label></ul>
				<? } ?>
			</ul>
		</div>
	<? } ?>
	
	<form method="POST" action="article_edit.php">
        <? if (isset($values["article_id"])) { ?>
			<input type="hidden" name="article_id" value="<?= $values["article_id"]; ?>" />
		<? } ?>
		
		<label for="article_author">Author:</label><br />
		<input type="text" name="article_author" id="article_author" value="<?= (isset($values["article_author"])) ? $values["article_author"] : ""; ?>" />
		<br /><br />
		
		<label for="article_title">Title:</label><br />
		<input type="text" name="article_title" id="article_title" value="<?= (isset($values["article_title"])) ? $values["article_title"] : ""; ?>" />
		<br /><br />
		
		<label for="article_content">Content:</label><br />
		<textarea name="article_content" id="article_content" style="width: 250px; height: 100px;"><?= (isset($values["article_content"])) ? $values["article_content"] : ""; ?></textarea>
		<br /><br />
		
		<input type="submit" value="Submit" />
		<a href="index.php" style="color: black; text-decoration: none;">Cancel</a>
    </form>
</body>
</html>