<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Week 8 Homework - Users List</title>
</head>

<body>
	<h2>Report</h2>
	
	<form method="GET" action="users_report.php">
		<label for="user_id">User ID: </label>
		<input type="number" name="user_id" id="user_id" min="0" value="<?= (isset($_GET["user_id"])) ? $_GET["user_id"] : ""; ?>" />
		
		<br />
		
		<label for="username">Username: </label>
		<input type="text" name="username" id="username" value="<?= (isset($_GET["username"])) ? $_GET["username"] : ""; ?>" />
		
		<br />
		
		<label for="user_level">User Level: </label>
		<input type="number" name="user_level" id="user_level" min="1" max="2" value="<?= (isset($_GET["user_level"])) ? $_GET["user_level"] : ""; ?>" />
		
		<br />
		
		<input type="submit" value="Submit" />
		<a href="users_report.php">Reset</a>
	</form>
	
	<br />
	
	<div>
		<a href="<?= $downloadLink; ?>">Download</a>
		<br /><br />
	</div>
	
	<table>
		<tr>
			<? foreach ($login->colsToDisplay as $header => $col) { ?>
				<th><?= $header; ?></th>
			<? } ?>
			<th colspan="2"></th>
		</tr>
		
		<? while ($row = $list->fetch(PDO::FETCH_ASSOC)) { ?>
			<tr>
				<? foreach ($login->colsToDisplay as $header => $col) { ?>
					<td><?= $row[$col]; ?></td>
				<? } ?>
			</tr>
		<? } ?>
	</table>
	
	<br />
	<a href="users_list.php">Back</a>
</body>
</html>