<?
require_once(dirname(__FILE__) . "/base.class.php");

class News_Article extends Base_Class {
	public $keyField;
	public $colsToDisplay;
	
	public function __construct() {
		parent::__construct("news_articles", "article_id");
		
		$this->keyField = "article_id";
		
		$this->colsToDisplay = [
			"Author"		=> "article_author",
			"Article Title"	=> "article_title",
			"Article Date"	=> "article_date"
		];
	}
	
	public function sanitize($data)
	{        
        return parent::sanitize($data);
	}
	
	public function validate()
	{
		return parent::validate();
	}
	
	/*public function getList($search = null) {
		$sql = "SELECT * FROM " . $this->tableName;
		$paramList = [];
		
		if (isset($search) && !empty($search))
		{
			$sql .= " WHERE article_author LIKE ? OR article_title LIKE ? OR article_content LIKE ?";
			$paramList[] = "%" . $search . "%";
			$paramList[] = "%" . $search . "%";
			$paramList[] = "%" . $search . "%";
		}
		
		$stmt = $this->db->prepare($sql);
		$stmt->execute($paramList);
		
		return $stmt;
	}*/
}
?>