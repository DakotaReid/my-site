<?
require_once dirname(__FILE__) . "/../inc/login.class.php";

$login = new Login();
$login_save = null;

if (!empty($_POST)) {
    $login_save = $login->save($_POST);	
    $error_output = $login->errors;
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Week 4 Homework</title>
</head>

<body>
    <? if ($login_save !== true) { ?>
	<form method="post">
        <? if (!empty($error_output)) { ?>
            <div class="errors">
                <p>Error(s):</p>
                <ul>
                    <? foreach ($error_output as $field => $error_msg) { ?>
                        <li><label for="<?= $field; ?>"><?= $error_msg; ?></label></li>
                    <? } ?>
                </ul>
            </div>
        <? } ?>
        
        <label for="email">Email:</label><br />
        <input type="text" name="email" id="email" value="<?= isset($_POST["email"]) ? $_POST["email"] : ""; ?>" />
		
        <br /><br />
		
        <label for="password">Password:</label><br />
        <input type="password" name="password" id="password" value="<?= isset($_POST["password"]) ? $_POST["password"] : ""; ?>" />
        
        <input type="submit" value="Login" />
    </form>
    <? } else { ?>
    <div>
        <p>This is the data stored in the $_SESSION variable:</p>
		
		<ul>
			<li><?= $_SESSION["email"]; ?></li>
			<li><?= $_SESSION["password"]; ?></li>
		</ul>
    </div>
    <? } ?>
</body>
</html>
