<?
session_start();
require_once dirname(__FILE__) . "/base.class.php";

class Login extends Base_Class {
    public function __construct() {
    	parent::__construct();
    }

    public function validate($data, $data_type = "text") {
        return parent::validate($data, $data_type);
    }

    public function load($id) {
        return parent::load($id);
    }

    public function save($data) {		
		$saved_data = parent::save($data);
		
		if ($saved_data) {
			$_SESSION["email"] = $data["email"];
			$_SESSION["password"] = $data["password"];
			
			return true;
		} else {
			return false;
		}
    }
}
?>