<?
require_once dirname(__FILE__) . "/base.class.php";

class Contact extends Base_Class {	
    public function __construct() {
    	parent::__construct();
    }

    public function validate($data, $data_type = "text") {
        return parent::validate($data, $data_type);
    }

    public function load($id) {
        return parent::load($id);
    }

    public function save($data) {		
		foreach ($data as $key => $value) {
			$valid = false;
			
			switch ($key) {
				case "date_of_birth": $valid = $this->validate($value, "date");
					break;
				case "email": $valid = $this->validate($value, "email");
					break;
				default: $valid = $this->validate($value);
			}
			
			if (!$valid) {
				$this->errors[$key] = ucwords(str_replace("_", " ", $key)) . " is invalid";
			}
		}
		
		if (empty($this->errors)) {
			//save to db or some other storage
			return true;
		}
    }
}
?>