<?
class Validate {
	public $data;
	
    public function __construct($input) {
        $this->data = $input;
    }

    public function validate_string() {        
        if (empty($this->data)) {
            return false;
        }
        
		$this->data = filter_var($this->data, FILTER_SANITIZE_STRING);
        
        return $this->data;
    }
	
	public function validate_date() {
		$this->data = $this->validate_string($this->data);
		
		if(!$this->data) {
			return false;
		}
		
		if (!preg_match("/^(19|20)\d\d[-.](0[1-9]|1[012])[-.](0[1-9]|[12][0-9]|3[01])$/", $this->data)) {
			return false;
		}
		
		return $this->data;
	}
	
	public function validate_email() {
		if (empty($this->data)) {
			return false;
		}
		
		$this->data = filter_var($this->data, FILTER_SANITIZE_EMAIL);
        $this->data = filter_var($this->data, FILTER_VALIDATE_EMAIL);
		
		return $this->data;
	}
}
?>