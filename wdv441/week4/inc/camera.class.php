<?
class Camera {
	var $aperture;
	var $lenseSize;
	var $resolution;
	
	public function getAperture() {
		return $this->aperture;
	}
	
	public function getLenseSize() {
		return $this->lenseSize;
	}
	
	public function getResolution() {
		return $this->resolution;
	}
}