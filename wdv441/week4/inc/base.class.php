<?
require_once dirname(__FILE__) . "/validate.class.php";

class Base_Class {
	public $errors;
	
    public function __construct() {
        $this->errors = [];
    }

    public function validate($data, $data_type = "text") {		
        $validate = new Validate($data);
		
		switch ($data_type) {
			case "date": $data = $validate->validate_date();
				break;
			case "email": $data = $validate->validate_email();
				break;
			default: $data = $validate->validate_string();
		}
		
		return $data;
    }

    public function load($id) {
        //Sanitize and validate INT
        if (!$this->validate($id)) {
            return false;
        }
        //Load from storage
        //Return keyed array of data
    }

    public function save($data) {
        foreach ($data as $key => $value) {
			$valid = false;
			
			switch ($key) {
				case "date": $valid = $this->validate($value, "date");
					break;
				case "email": $valid = $this->validate($value, "email");
					break;
				default: $valid = $this->validate($value);
			}
			
			if (!$valid) {
				$this->errors[$key] = ucwords(str_replace("_", " ", $key)) . " is invalid";
			}
		}
		
		if (empty($this->errors)) {
			//save to db or some other storage
			return true;
		}
    }
}
?>