<?
class Base_Class {
    public function __construct() {
        
    }

    public function validate($data, $data_type = "text") {        
        if (empty($data)) {
            return false;
        }
        
        switch ($data_type) {
            case "date":
                    $data = filter_var($data, FILTER_SANITIZE_STRING);                
                    $data = explode("-", $data);
                                    
                    if (!checkdate($data[1], $data[2], $data[0])) {
                        return false;
                    }
                break;
            case "email":
                    $data = filter_var($data, FILTER_SANITIZE_EMAIL);
                    $data = filter_var($data, FILTER_VALIDATE_EMAIL);
                break;
            default:
                $data = filter_var($data, FILTER_SANITIZE_STRING);
        }
        
        return $data;
    }

    public function load($id) {
        //Sanitize and validate INT
        if (!$this->validate($id)) {
            return false;
        }
        //Load from storage
        //Return keyed array of data
    }

    public function save($data) {
        //Validate the data
        if ($this->validate($data)) {
            //Save to long term storage
        } else {
            return false;
        }
    }
}
?>