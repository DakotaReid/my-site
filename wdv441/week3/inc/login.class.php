<?
require_once dirname(__FILE__) . "/base.class.php";

class Login extends Base_Class {
    public function __construct() {
        parent::__construct();
    }
	
	public function sanitizeData($data) {
		//sanitize data for validation and saving to the db
		return $data;
	}

    public function validate($data, $data_type = "text") {
        //sanitate/validate the data
		$validateData = validateData($data);	//validateData runs sanitizeData
		
		//if data is valid return true, else return/send error msg
		if ($validateData) {
			return true;
		} else {
			$errMsg["field"] = "<field> is invalid";
		}
		
		return $errMsg;
    }

    public function load($id) {
		//validate id
		validateData($data);
		
		//load from login db using id
		$userInfo = loadLoginInfo($id);
		
		//if successful return first name, last name, email address, phone number else return false
		if ($userInfo) {
			return $userInfo;
		} else {
			return false;
		}
    }
	
	public function loadEmailAndPass($emailAddress, $password) {
		//load from login db using email address and password
		$userInfo = loadLoginInfo($emailAddress, $password);
		
		//if successful return first name, last name, email address, phone number else return false
		if ($userInfo) {
			return $userInfo;
		} else {
			return false;
		}
	}

    public function save($data) {
		//validate data
		validateData($data);
		
        //insert into login db first name, last name, email address, phone number, password
		$save = saveLoginInfo($data);
		
		//returns true if successful
		if ($save) {
			return true;
		} else {
			return false;
		}
    }
	
	public function login($emailAddress, $password) {
		//validate email address and password
		$data = [$email_address, $password];
		
		//if validation fails return false
		if (validateData($data) !== true) {
			return false;
		}
		
		//set session variables from login db using email address and password for first name, last name, email address, phone number
		$userInfo = loadEmailAndPass($emailAddress, $password);
		
		//if successful return true, else return false
		if ($userInfo) {
			foreach ($userInfo as $key => $value) {
				$_SESSION[$key] = $value;
			}
			
			return true;
		} else {
			return false;
		}
    }
}
?>