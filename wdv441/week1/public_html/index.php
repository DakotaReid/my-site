<?
require_once "../inc/func.inc.php";

$randNumIsLess = false;

if ($randNum < count($nameList)) {
    $randNumIsLess = true;
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Week 1 Homework</title>
</head>

<body>
	<? if ($randNumIsLess) { ?>
        <p>Hello <?= $nameList[$randNum]; ?>.</p>
    <? } else { ?>
        <p>Name List:</p>
        <ol>
            <? foreach ($nameList as $key => $name) { ?>
                <li><?= $name; ?></li>
            <? } ?>
        </ol>
    <? } ?>
</body>
</html>
