<?
require_once(dirname(__DIR__) . "/inc/news.class.php");
$searchVal = null;

$article = new News_Article();

if (!empty($_GET["searchVal"]) && $_GET["searchVal"] != "")
{
	$searchVal = filter_var($_GET["searchVal"], FILTER_SANITIZE_STRING);
}

$list = $article->getList($searchVal);

include_once(dirname(__DIR__) . "/tpl/index.tpl.php");
?>