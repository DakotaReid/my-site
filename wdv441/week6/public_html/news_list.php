<?
require_once(dirname(__FILE__) . "/../inc/news.class.php");

$article = new News_Article();

$list = $article->getList();
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Week 6 Homework - News List</title>
</head>

<body>
	<h2>Articles</h2>
	
	<? if (isset($_GET["saved_article"]) && $_GET["saved_article"]) { ?>	
		<div>
			<p>Article has been saved.</p>
		</div>
	<? } ?>
	
	<a href="article_edit.php">Create new Article</a>
	
	<br /><br />
	
	<table>
		<tr>
			<th>Author</th>
			<th>Title</th>
			<th>Content</th>
			<th>Date</th>
			<th>Edit</th>
		</tr>
		
		<? while ($row = $list->fetch(PDO::FETCH_ASSOC)) { ?>
			<tr>
				<td><?= $row["article_author"]; ?></td>
				<td><?= $row["article_title"]; ?></td>
				<td><?= $row["article_content"]; ?></td>
				<td><?= date("m/d/Y h:i A", strtotime($row["article_date"])); ?></td>
				<td><a href="article_edit.php?article_id=<?= $row["article_id"]; ?>">Edit</a></td>
			</tr>
		<? } ?>
	</table>
</body>
</html>