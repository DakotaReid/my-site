<?
require_once(dirname(__DIR__) . "/inc/news.class.php");

$article = new News_Article();

if (!empty($_GET["article_id"]))
{
	$article->load($_GET["article_id"]);
}

include_once(dirname(__DIR__) . "/tpl/article_view.tpl.php");
?>