<?
require_once(dirname(__DIR__) . "/inc/login.class.php");

$login = new Login();

if (isset($_REQUEST["user_id"]) && $_REQUEST["user_id"] > 0)
{
	$login->load($_REQUEST["user_id"]);
}

if (!empty($_POST))
{
	if ($login->set($_POST))
	{
		if ($login->validate())
		{
			if ($login->save())
			{
				$login->saveUploadedImage($_FILES);
				
				redirect("index.php?saved_user=true");
			}
		}
	}
}

$values = $login->data;

include_once(dirname(__DIR__) . "/tpl/user_edit.tpl.php");
?>