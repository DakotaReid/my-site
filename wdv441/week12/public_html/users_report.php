<?
require_once(dirname(__DIR__) . "/inc/login.class.php");

$login = new Login();
$rightsRequired = 2;

if (empty($_SESSION) || empty($_SESSION["credentials"]) && $login->checkRights($_SESSION["credentials"]["user_id"], $rightsRequired))
{
	redirect("users_list.php");
}

if (isset($_GET["download"]) && $_GET["download"])
{
	unset($_GET["download"]);
	
	$list = $login->generateReport($_GET, true);
	exit;
} else {
	$list = $login->generateReport($_GET);
}

$downloadLink = "users_report.php?download=true";
foreach ($_GET as $key => $value)
{
	$downloadLink .= "&" . $key . "=" . $value;
}

include_once(dirname(__DIR__) . "/tpl/users_report.tpl.php");
?>