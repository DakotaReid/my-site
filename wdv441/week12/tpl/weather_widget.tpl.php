<div>
	<h2>Current Weather Conditions in Ankeny</h2>
	
	<!--<img src="images/weather_icons/<?= str_replace(" ", "", $current_weather["weather_text"]); ?>.gif" />-->
	
	<p><?= ucwords($current_weather["weather_text"]); ?></p>
	<p>Temperature: <?= $current_weather["temp"]; ?>&deg;<?= strtoupper($current_weather["temp_unit"]); ?></p>
	<p>Humidity: <?= $current_weather["humidity"]; ?>%</p>
	<p>Barometric Pressure: <?= $current_weather["pressure"]; ?> mb</p>
	
	<br />
	
	<p>Wind Direction: <?= $current_weather_wind["dir"]; ?></p>
	<p>Wind Speed: <?= $current_weather_wind["speed"]; ?> <?= $current_weather_wind["wind_unit"]; ?></p>
</div>