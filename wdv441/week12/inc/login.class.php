<?
require_once(dirname(__FILE__) . "/base.class.php");

class Login extends Base_Class {
	public $keyField;
	public $colsToDisplay;
	public $searchableFields;
	private $AESKeyStr;
	
	public function __construct()
	{
		parent::__construct("login", "user_id");
		
		$this->keyField = "user_id";
		
		$this->colsToDisplay = [
			"Username"		=> "username",
			"Password"		=> "password",
			"User Level"	=> "user_level"
		];
		
		$this->searchableFields = [
			"Username"		=> "username",
			"User Level"	=> "user_level"
		];
		
		$this->AESKeyStr = "hwrpZs6VwonpY4fH";
	}
	
	public function getNextUserID()
	{
		$sql = "SELECT MAX(user_id) as max_ID FROM " . $this->tableName;
		
		$maxID = 1;
		foreach ($this->db->query($sql) as $row)
		{
			$maxID = $row["max_ID"];
		}
		
		return ($maxID + 1);
	}
	
	public function save()
	{
		$saved = true;
		$saveValues = "";
		$sql = "";
		$values = [];
		
		$count = 0;
		foreach ($this->data as $name => $value)
		{
			$saveValues .= ($count++ > 0 ? ", " : "") . $name . " = ?";
			$values[] = $value;
		}
		
		if (isset($this->data[$this->keyField]) && !empty($this->data[$this->keyField]))
		{
			$sql = "UPDATE " . $this->tableName . " SET ";
			$sql .= $saveValues;
			$sql .= " WHERE " . $this->keyField . " = ?";

			$values[] = $this->data[$this->keyField];
		} else {
			$sql = "INSERT INTO " . $this->tableName . " SET ";
			$sql .= $saveValues;
		}
		
		$stmt = $this->db->prepare($sql);
		
		if (!$stmt->execute($values))
		{
			$saved = false;
			$this->errors[] = $stmt->errorInfo();
		}
		
		return $saved;
	}
	
	public function checkCredentials()
	{		
		$stmt = $this->db->prepare("SELECT * FROM " . $this->tableName . " WHERE username = ? AND password = ?");
		
		$stmt->execute(array_values($this->data));
		
		if ($stmt->rowCount() >= 1)
		{
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
			{
				$_SESSION["credentials"] = [
					"user_id"		=> $row["user_id"],
					"username"		=> $row["username"]
				];
			}
			
			return true;
		}
		
		$this->errors[] = "Username or Password is incorrect";
		return false;
	}
	
	public function checkRights($user_id, $user_level)
	{
		$hasRights = false;
		
		$login = new Login();
		
		if ($login->load($user_id))
		{
			$hasRights = ($login->data["user_level"] == $user_level);
		}
		
		return $hasRights;
	}
}
?>