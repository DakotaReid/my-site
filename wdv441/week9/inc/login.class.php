<?
require_once(dirname(__FILE__) . "/base.class.php");

class Login extends Base_Class {
	public $colsToDisplay;
	public $searchableFields;
	private $AESKeyStr;
	
	public function __construct()
	{
		parent::__construct("login", "user_id");
		
		$this->colsToDisplay = [
			"Username"		=> "username",
			"Password"		=> "password",
			"User Level"	=> "user_level"
		];
		
		$this->searchableFields = [
			"Username"		=> "username",
			"User Level"	=> "user_level"
		];
		
		$this->AESKeyStr = "hwrpZs6VwonpY4fH";
	}
	
	public function getNextUserID()
	{
		$sql = "SELECT MAX(user_id) as max_ID FROM " . $this->tableName;
		
		$maxID = 1;
		foreach ($this->db->query($sql) as $row)
		{
			$maxID = $row["max_ID"];
		}
		
		return ($maxID + 1);
	}
	
	public function save()
	{
		$saved = true;
		$saveValues = "";
		$sql = "";
		$values = [];
		
//		if (!empty($_FILES))
//		{
//			$avatar_dir = dirname(__DIR__) . "/public_html/avatars/";
//			
//			var_dump($_FILES);
//			
//			if (!empty($_FILES["user_avatar"]["name"]))
//			{
//				$user_id = (isset($_REQUEST["user_id"])) ? $_REQUEST["user_id"] : $this->getNextUserID();
//				
//				$file_pathinfo = pathinfo($_FILES["user_avatar"]["name"]);
//				$target_file = $avatar_dir . "/" . $user_id . "." . $file_pathinfo["extension"];
//				//$valid_pic_extentions = ["gif", "jpeg", "png"];
//				$valid_pic_extentions = ["png"];
//				
//				if (in_array($file_pathinfo["extension"], $valid_pic_extentions))
//				{
//					if (!move_uploaded_file($_FILES["user_avatar"]["tmp_name"], $target_file))
//					{
//						$this->errors[] = "Unable to Upload Your File";
//					}
//				}
//			}
//		}
		
		$count = 0;
		foreach ($this->data as $name => $value)
		{
			$saveValues .= ($count++ > 0 ? ", " : "") . $name . " = ?";
			$values[] = $value;
		}
		
		if (isset($this->data[$this->keyField]) && !empty($this->data[$this->keyField]))
		{
			$sql = "UPDATE " . $this->tableName . " SET ";
			$sql .= $saveValues;
			$sql .= " WHERE " . $this->keyField . " = ?";

			$values[] = $this->data[$this->keyField];
		} else {
			$sql = "INSERT INTO " . $this->tableName . " SET ";
			$sql .= $saveValues;
		}
		
		$stmt = $this->db->prepare($sql);
		
		if (!$stmt->execute($values))
		{
			$saved = false;
			$this->errors[] = $stmt->errorInfo();
		}
		
		return $saved;
	}
	
	public function checkCredentials()
	{		
		$stmt = $this->db->prepare("SELECT * FROM " . $this->tableName . " WHERE username = ? AND password = ?");
		
		$stmt->execute(array_values($this->data));
		
		if ($stmt->rowCount() >= 1)
		{
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
			{
				$_SESSION["credentials"] = [
					"user_id"		=> $row["user_id"],
					"username"		=> $row["username"],
					"user_level"	=> $row["user_level"]
				];
			}
			
			return true;
		}
		
		$this->errors[] = "Username or Password is incorrect";
		return false;
	}
	
	public function uploadAvatar()
	{
		
	}
}
?>