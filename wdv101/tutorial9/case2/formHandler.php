﻿<!DOCTYPE html>
<html>
<head>
	<meta name="author" content="Dakota Reid - Date: 11/23/14" />
    <title></title>
	<link rel="stylesheet" href="foodstyles.css" type="text/css" />
</head>

<body>

<section id="container">
        <header>
            <p><img src="images/foodlogo.jpg" alt="image of Onloin foods logo" /></p>
        </header>

		<section id="info">
            <h3>Get Onloin Online</h3>
            <p>We have the finest foods available anywhere. We will ship to your home or business within the continental United States and Canada. With our express shipping, you will get your order the next day. Our customers have been shopping with us since 1995.</p>

            <p>We have more than 50 years' experience in the restaurant and food business. We know fresh and we know quality, and we can give you both online and delivered right to your doorstep.</p>

            <p>Fill out the form at right and become one of our customers today. You'll receive e-mail alerts whenever we have a special sale. Save big each month on all our food products.</p>

            <p>Sign up now for automated ordering. We'll ship you the foods you want each month, and we'll do so at a discount to you, our best customers.</p>

            <p>You won't have to fill out orders anymore with our automated ordering. Get the finest and freshest foods delivered without any delay.</p>

            <p>Get OnLoin now!</p>
        </section>

		<section id="order">
            <?php
			$body = "Form Data\n\n";
			foreach($_POST as $key => $value)
			{
				$body .= $key."=".$value."\n";
			}

			echo "<table border='1'>";
			echo "<tr><th>Field Name</th><th>Value of field</th></tr>";
			foreach($_POST as $key => $value)
			{
				echo '<tr>';
				echo	'<td>',$key,'</td>';
				echo	'<td>',$value,'</td>';
				echo "</tr>";
			}
			echo "</table>";
		?>
        </section>

        <footer>
			<p><a href="food_T9.htm">Home</a></p>
            <p><a href="#">Service</a> | <a href="#">Gift Center </a>| <a href="#">Contact Us</a> | <a href="#">Order Tracking </a></p>
            <p>Prefer to phone in your order? Call 1-877 ONL-FOOD</p>
        </footer>
    </section>
</body>
</html>
