var http = require("http");
var fs = require("fs");

var server = http.createServer(function(request, response){

	request.on("end", function() {
		response.writeHead(200, {"Content-Type": "text/plain"});
	});
	
	response.write("Hello World!");
	
	response.end("This is a message.");
	
//	request.on("end", function() {		
//		fs.readFile("test.txt", "utf-8", function (error, data) {
//			response.writeHead(200, {"Content-Type": "text/plain"});
//			
//			data = parseInt(data) + 1;
//			
//			//fs.writeFile("test.txt", data);
//			
//			response.end("This page has been accessed " + data + " times.");
//		});
//	});
}).listen(81);

//If no encoding is specified, then the raw buffer is returned.
fs.readFile("test.txt", "utf-8", function (error, data) {
	if (error) {
		throw err;
	}
	
	console.log(data);
});