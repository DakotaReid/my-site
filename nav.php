<?php
	$active = 'class="active"';

	var_dump($active);

	function getUrl() {
		$url = $_SERVER["REQUEST_URI"];
	
		return $url;
	}
?>

<nav id="cssmenu">
    <ul>
        <li>
			<? if (getUrl() == "/index.php") { ?>
				<a href="/index.php" <?= $active; ?>>Dakota Reid</a></li>
        <li>
            <a href="hobbies.html">Hobbies</a>
            <ul>
                <li><a href="hobbies.html#photography">Photography</a></li>
            </ul>
        </li>
        <li>
            <a href="projects.html">Projects</a>
            <ul>
                <li><a href="projects.html#cpp">C++</a></li>
                <li><a href="projects.html#java">Java</a></li>
                <li><a href="projects.html#javascript">JavaScript</a></li>
            </ul>
        </li>
        <li>
            <a href="webdev.html">Web Development</a>
            <ul>
                <li><a href="wdv101.html">WDV101</a></li>
                <li><a href="wdv341.html">WDV341</a></li>
                <li><a href="wdv221.html">WDV221</a></li>
            </ul>
        </li>
    </ul>
</nav>