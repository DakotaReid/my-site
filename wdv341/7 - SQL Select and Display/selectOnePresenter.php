<!DOCTYPE html>
<html>
<head>
	<title>WDV341 Into PHP  - Presenters CMS Example</title>

	<?php
		include "../dbConnect.php";				//connects to the database

		$sql = "SELECT * FROM wdv341_presenters WHERE presenter_id = 1";		//build the SQL query
							//Note the WHERE clause allows us to select ONLY the desired record
		
		$result = mysqli_query($link, $sql);		//run the Query and store the result in $result

		if(!$result)							//Make sure the Query ran correctly and created result
		{
			echo "<h1 style='color:red'>Houston, We have a problem!</h1>";	//Problems were encountered.
			echo mysqi_error($link);		//Display error message information
		}
	?>
</head>

<body>
	<h1>WDV341 Intro PHP</h1>
	<h2>Presenters CMS Example</h2>
	<h3>View One Presenter</h3>
	<p>This page is very similar to the multiple presenters view. However this one will pull ONE of the presenters from the presenters table in the database. It will display all of the columns for that presenter as an HTML table. </p>
	<p>NOTE: For purposes of this example hard code one of your presenter_ids into the WHERE clause. </p>
	<p>The SQL SELECT query will use the WHERE clause to filter the selection to the row that has a specific presenter_if value. This allows us to select a specific record within the table of the database. The WHERE clause can also be used in variety of comparisons on all the columns in the table. For example: WHERE presenter_city = &quot;Des Moines&quot;, or WHERE presenter_st &lt;&gt; IA. </p>
	<p>Each presenter has an Update link. The update link will call a form page that uses PHP to fill out the form for the chosen presenter. Notice how the link passes the presenter_id as a GET parameter on the URL in the href attribute of the hyperlink element.</p>
	<p>Each presenter has a Delete link. The delete link will call a php page that will delete the selected record from the table. Notice how the link passes the presenter_id as a GET parameter on the URL in the href attribute of the hyperlink element. </p>
	
	<?php
		echo "<h3>" . mysqli_num_rows($result). " records were found.</h3>";	//display number of rows found by query
	?>

	<div>
		<table border="1">
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>City</th>
				<th>State</th>
				<th>Zip</th>
				<th>Email Address</th>
				<th>Update</th>
				<th>Delete</th>
			</tr>

		<?php
			while($row = mysqli_fetch_array($result))		//Turn each row of the result into an associative array 
  			{
				//For each row you found int the table create an HTML table in the response object
  				echo "<tr>";
  				echo "<td>" . $row['presenter_first_name'] . "</td>";
  				echo "<td>" . $row['presenter_last_name'] . "</td>";
  				echo "<td>" . $row['presenter_city'] . "</td>";
  				echo "<td>" . $row['presenter_st'] . "</td>";
  				echo "<td>" . $row['presenter_zip'] . "</td>";
  				echo "<td>" . $row['presenter_email'] . "</td>";
				echo "<td><a href='presentersUpdateForm.php?recId=" . $row['presenter_id'] . "'>Update</a></td>";
				echo "<td><a href='deletePresenters.php?recId=" . $row['presenter_id'] ."'>Delete</a></td>";
  				echo "</tr>";
  			}

			mysqli_close($link);		//Close the database connection to free up server resources.
		?>
		</table>
	</div>	
</body>
</html>