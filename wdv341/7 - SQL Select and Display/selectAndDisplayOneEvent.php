<!DOCTYPE html>
<html>
<head>
	<title>Select and Display One Event</title>

	<?php
		include "../dbConnect.php";

		$sql = "SELECT * FROM wdv341_events WHERE event_id = 1";
		$result = mysqli_query($link, $sql);

		if(!$result)
		{
			echo mysqi_error($link);
		}
	?>
</head>

<body>
	<div>
		<table border="1">
			<tr>
				<th>ID</th>
				<th>Event Name</th>
				<th>Description</th>
				<th>Presenter</th>
				<th>Day</th>
				<th>Time</th>
			</tr>

		<?php
			while($row = mysqli_fetch_array($result))
  			{
  				echo "<tr>";
  				echo "<td>" . $row['event_id'] . "</td>";
  				echo "<td>" . $row['event_name'] . "</td>";
  				echo "<td>" . $row['event_description'] . "</td>";
  				echo "<td>" . $row['event_presenter'] . "</td>";
  				echo "<td>" . $row['event_day'] . "</td>";
  				echo "<td>" . $row['event_time'] . "</td>";
  				echo "</tr>";
  			}

			mysqli_close($link);
		?>
		</table>
	</div>	
</body>
</html>