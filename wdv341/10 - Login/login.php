<?php session_start(); ?>

<?php include ('loginFunctions.php'); ?>

<!DOCTYPE html>
<html>
<head>
    <title>Event Login and Control Page</title>
	<link rel="icon" href="../../images/shield.ico" />
    <link rel="stylesheet" href="../../css/style.css" />
	<style>
		#content > ul > li {
			padding-bottom: 1em;
		}

		#content > ul > li a:link,
		#content > ul > li a:active,
		#content > ul > li a:visited {
			font-family: Verdana, Arial, Sans-Serif;
			background: #333;
			color: #ccc;
		}

		#content > ul > li a:hover {
			text-decoration: none;
		}

		form {
			width: 28%;
		}

		label {
			width: 5.5em;
			margin-right: 1em;
			float: left;
		}

		.textbox {
			display: block;
		}
	</style>
</head>

<body>
    <div id="wrapper">
        <div id="cssmenu">
            <ul>
				<?php
					if(isValidUser()) {
				?>
				<li><a href="login.php" class="active">Admin Options</a></li>
                <li><a href="../Events/insertEvent.php">Insert</a></li>
                <li><a href="../Events/displayEvents.php">Display/Modify</a></li>
				<li style="float: right;"><a href="logout.php" style="margin-right: 0;">Logout</a></li>
				<?php
					}
					else {
				?>
				<li><a href="../../wdv341.html">WDV341</a></li>
				<li style="float: right;"><a href="login.php" class="active" style="margin-right: 0;">Login</a></li>
				<?php
					}
				?>
            </ul>
        </div>

		<div id="content">
		<?php
			displayOptionsOrForm();
		?>
			<ul style="clear: both;"><li><a href="../../wdv341.html">Return to the WDV341 Page</a></li></ul>
		</div>
	</div>
</body>
</html>