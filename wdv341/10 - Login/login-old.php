<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
	<title>WDV341 Intro PHP - Login and Control Page</title>
	<link rel="stylesheet" href="../../style.css" />
</head>

<body>
	<div id="wrapper">
		<div id="cssmenu">
			<ul>
				<li><a href="index.html">Dakota Reid</a></li>
				<li>
					<a href="hobbies.html">Hobbies</a>
					<ul>
						<li><a href="hobbies.html#photography">Photography</a></li>
					</ul>
				</li>
				<li>
					<a href="projects.html">Projects</a>
					<ul>
						<li><a href="projects.html#cpp">C++</a></li>
						<li><a href="projects.html#java">Java</a></li>
						<li><a href="projects.html#javascript">JavaScript</a></li>
					</ul>
				</li>
				<li>
					<a href="webdev.html" class="active">Web Development</a>
					<ul>
						<li><a href="wdv101.html">WDV101</a></li>
						<li><a href="wdv341.html">WDV341</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div id="content">
			<?php
				if($_SESSION['validUser'] == "yes") {
			?>
					<h1>Admin Options</h1>
					<ul>
						<li><a href="inputEntry.html">Input New Entry</a></li>
						<li><a href="updateEntry.php">Update Entry</a></li>
						<li><a href="deleteEntry.php">Delete Entry</a></li>
						<li><a href="logout.php">Logout of Admin System</a></li>
					</ul>
			<?php
				}
				else {
					if(isset($_POST['submitLogin'])) {
						$inUsername = $_POST['loginUsername'];
						$inPassword = $_POST['loginPassword'];
			
						include ('../dbConnect.php');
			
						$sql = "SELECT * FROM event_users WHERE event_username = '" . $inUsername . "' AND event_password = '" . $inPassword . "'";

						$result = mysqli_query($link, $sql);

						if(mysqli_num_rows($result) == 1) {
							$_SESSION['validUser'] = "yes";
			?>
							<h1>Admin Options</h1>
       		 				<a href="inputEntry.html">Input New Entry</a>
        					<a href="updateEntry.php">Update Entry</a>
        					<a href="deleteEntry.php">Delete Entry</a>
        					<a href="logout.php">Logout of Admin System</a>
			<?php
						}
						else {
							echo "<h3>Invalid username or password.  Please try again.</h3>";
			?>
							<form method="post" name="login" action="login.php">
							  <p>Username: <input name="loginUsername" type="text" /></p>
							  <p>Password: <input name="loginPassword" type="password" /></p>
							  <p><input name="submitLogin" value="Login" type="submit" /> <input name="" type="reset" />&nbsp;</p>
							</form>
			<?php
						}
					}
					else {
			?>
						<h1>Please login to the Admin System</h1>
						<form method="post" name="loginForm" action="login.php">
	  						<p>Username: <input name="loginUsername" type="text" /></p>
	  						<p>Password: <input name="loginPassword" type="password" /></p>
	  						<p><input name="submitLogin" value="Login" type="submit" /> <input name="" type="reset" />&nbsp;</p>
						</form>
			<?php
					}
				}
			?>

			<ul><li><a href="login.php">Return to Login Page</a></li></ul>
		</div>
	</div>
</body>
</html>