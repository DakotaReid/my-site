<?php
	function displayAdminOptions() {
		echo "<h1>Event Admin Options</h1>";
		echo "<ul>";
		echo	"<li><a href='../Events/insertEvent.php'>Insert New Event</a></li>";
		echo	"<li><a href='../Events/displayEvents.php'>Display/Modify Event List</a></li>";
		echo	"<li><a href='logout.php'>Logout of Admin System</a></li>";
		echo "</ul>";
	}

	function displayLoginForm() {
		echo "<h1>Please login to the Admin System</h1>";
		echo "<form method='post' name='loginForm' action='login.php'>";
		echo	"<label for='user'>Username:</label>";
		echo	"<input name='loginUsername' type='text' name='user' required='required' />";
		echo	"<label for='pass'>Password:</label>";
		echo	"<input name='loginPassword' type='password' name='pass' required='required' />";
		echo	"<input name='submitLogin' value='Login' type='submit' style='float: right;' /> <input name='resetLogin' type='reset' style='margin-right: 1em; float: right;' />";
		echo "</form>";
	}

	function displayInvalidUserOrPass() {
		echo "<h1>Please login to the Admin System</h1>";
		echo "<h3>Invalid username or password</h3>";
		echo "<form method='post' name='loginForm' action='login.php'>";
		echo	"<label for='user'>Username:</label>";
		echo	"<input name='loginUsername' type='text' name='user' required='required' />";
		echo	"<label for='pass'>Password:</label>";
		echo	"<input name='loginPassword' type='password' name='pass' required='required' />";
		echo	"<input name='submitLogin' value='Login' type='submit' style='float: right;' /> <input name='resetLogin' type='reset' style='margin-right: 1em; float: right;' />";
		echo "</form>";
	}

	function isValidUser() {
		if($_SESSION['validUser'] == "yes") {
			return true;
		}
		else if(isset($_POST['submitLogin'])) {
			include ('../dbConnectAdmin.php');

			$inUsername = $_POST['loginUsername'];
			$inPassword = $_POST['loginPassword'];
			
			$sql = "SELECT * FROM event_users WHERE event_username = '" . $inUsername . "' AND event_password = '" . $inPassword . "'";

			$result = mysqli_query($link, $sql);

			if(mysqli_num_rows($result) == 1) {
				$_SESSION['validUser'] = "yes";
				$_SESSION['username'] = $_POST['loginUsername'];
				$_SESSION['password'] = $_POST['loginPassword'];

				return true;
			}
			else {
				return false;
			}
			mysqli_close($link);
		}
		return false;
	}

	function displayOptionsOrForm() {
		if(isValidUser()) {
			displayAdminOptions();
		}
		else {
			if(!isset($_POST['submitLogin'])) {
				displayLoginForm();
			}
			else {
				displayInvalidUserOrPass();
			}
		}
	}
?>