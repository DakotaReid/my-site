<?php session_start(); ?>

<?php
	if($_SESSION['validUser'] != "yes") {
		header("Location: login.php");
	}
	else {
		session_destroy();
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Logout Page</title>
	<link rel="icon" href="../../images/logout.ico" />
	<link rel="stylesheet" href="../../css/style.css" />
	<style>
		#content > ul > li {
			padding-bottom: 1em;
		}

		#content > ul > li a:link,
		#content > ul > li a:active,
		#content > ul > li a:visited {
			font-family: Verdana, Arial, Sans-Serif;
			background: #333;
			color: #ccc;
		}

		#content > ul > li a:hover {
			text-decoration: none;
		}
	</style>
</head>

<body>
	<div id="wrapper">
		<div id="cssmenu">
			<ul>
				<li><a href="../../wdv341.html">WDV341</a></li>
				<li style="float: right;"><a href="login.php" style="margin-right: 0;">Login</a></li>
			</ul>
		</div>

		<div id="content">
			<h1>You have been logged out.</h1>

			<ul><li><a href="login.php">Return to Login Page</a></li></ul>
		</div>
	</div>
</body>
</html>