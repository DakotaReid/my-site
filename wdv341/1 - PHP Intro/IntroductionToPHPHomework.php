﻿<!DOCTYPE html>
<html>
<head>
    <title>WDV341 Intro to PHP - Introduction to PHP</title>
</head>

<body>
    <h2>WDV341 Intro PHP</h2>
    <h3>Introduction to PHP - Homework</h3>
    <p>Complete the following exercises. When finished upload to your server. Update your links on your WDV341 Homework page to include this assignment. </p>
    <p>Zip your work and attach it to your Blackboard assignment and submit it to Blackboard.</p>
    <p>
        <br />
        1. Use this page as the source and convert it so that it will process your PHP code on the server.
    </p>
    <p>2. Use PHP to fill in the following information. </p>

    <?php
		echo "<p>Course Name: WDV341</p>
			 <h2>First Name: Dakota</h2>
			 <h2>Last Name: Reid</h2>";
	?>

    <p>My homework is located at: <a href="http://dakotareid.info/wdv341.html/">WDV341 Homework page</a></p>

    <p>3. Create an ordered list using PHP. Place it in the area below. It should describe the sequence of steps that the server and PHP processor performs when you request this page until you see the results. The more detailed the better!</p>
    
	<?php
		echo "<ol>
				 <li>Client requests the page from the server.</li>
				 <li>Server reads the requested file.</li>
				 <li>Server ignores any text that isn't PHP and sends it to the Client.</li>
				 <li>Server processes the PHP.</li>
				 <li>Server then continues to read the file.</li>
				 <li>Server then repeats steps 3 through 5 if it has not reached the end of the file or found more PHP code.</li>
				 <li>Server then sends the file with only HTML, CSS, and/or JavaScript.</li>
			 </ol>"
	?>

    <p> Each exercise is graded seperately and totaled for the Project grade. Each exercise will be graded using the following scale. Exercises 2 and 3 are worth 10 points each.</p>
    <table width="530" border="1">
        <tr>
            <td width="138">Grade</td>
            <td width="376">Expectation</td>
        </tr>
        <tr>
            <td>5</td>
            <td>Works as expected. Correctly uses concepts presented in class. Documented as needed and well formatted.</td>
        </tr>
        <tr>
            <td>4</td>
            <td>Works as expected. Uses some of the concepts presented in class. Lacks documentation if needed or poorly formatted/structured.</td>
        </tr>
        <tr>
            <td>2</td>
            <td>Produces results but not as expected OR Exercise is attempted but produces an error.</td>
        </tr>
        <tr>
            <td>0</td>
            <td>Does not produce any results. Fails to use any concepts presented in class or not attempted.</td>
        </tr>
    </table>
</body>
</html>
