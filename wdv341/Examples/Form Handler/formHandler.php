<!DOCTYPE html>
<html>
<head>
	<title>WDV101 Basic Form Handler Example</title>
</head>

<body>
	<h1>WDV101 Intro HTML and CSS</h1>
	<h2>Chapter 9 - Creating Forms</h2>

	<p><strong>PHP Form Handler</strong> - This process will display the 'name = value' pairs for all the elements of a form. Use formHandler.php in the action attribute of your form. </p>
	<p>Field '<strong>name</strong>' - The value of the name attribute from the HTML form element.</p>
	<p>Field '<strong>value</strong>' - The value entered in the field. This will vary depending upon the HTML form element.</p>
	<h3>Form Name-Value Pairs</h3>

	<?php
		echo "<table border='1'>";
		echo "<tr><th>Field Name</th><th>Value of field</th></tr>";
		foreach($_POST /*or $_GET*/ as $key => $value)
		{
			echo '<tr>';
			echo	'<td>',$key,'</td>';
			echo	'<td>',$value,'</td>';
			echo "</tr>";
		}
		echo "</table>";
		echo "<p>&nbsp;</p>";
	?>

</body>
</html>
