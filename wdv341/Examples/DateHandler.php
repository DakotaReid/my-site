<!DOCTYPE html>
<html>
<head>
	<title>Date Handling</title>
</head>

<body>
	<h1>Project 3 - Date Handling</h1>

	<?php
		echo date('n\/j\/Y');
		echo "<br />";
		echo date('d\/m\/Y');
		echo "<br />";
		echo "One week from today will be: " . date('n\/j\/Y', strtotime("+1 week"));
		echo "<br />";
		echo "30 days from today will be: " . date('n\/j\/Y', strtotime("+30 day"));
		echo "<br />";
	?>

	<?php
		$datetime1 = date_create('9/18/2014');
		$datetime2 = date_create('10/7/2014');
		$interval = date_diff($datetime1, $datetime2);

		echo "The number of days between 9/18/2014 and 10/7/2014 is: ";
		echo $interval->format('%R%a days');
		echo "<br />";
	?>

	<?php
		$dateOne = new DateTime('9/18/2014', new DateTimeZone('America/Chicago'));
		$dateTwo = new DateTime('10/7/2014', new DateTimeZone('America/Chicago'));
		$days = $dateOne->diff($dateTwo);
		
		echo "The number of days between 9/18/2014 and 10/7/2014 is: ";
		echo $days->format('%R%a days');
	?>

</body>
</html>