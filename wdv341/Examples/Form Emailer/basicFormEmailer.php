<!DOCTYPE html>
<html>
<head>
	<title>WDV111 Basic Form Handler Example</title>
</head>

<body>
	<h1>WDV101 Intro HTML and CSS</h1>
	<h2>Chapter 9 - Creating Forms</h2>

	<p>This process will put the 'name = value' pairs for all the elements of a form on an email.  The email will be sent to whatever email address you wish to use.  You can hard code the email address within the PHP code or you could pass the email address as part of the form content.</p>
	<p>Use basicFormEmailer.php in the action attribute of your form. Upload this PHP file to your server.  This page will display the form content and send it to the indicated email address. </p>

	<h3>you must change the $to variable within the PHP code to your email address.</h3>
	<p>Field '<strong>name</strong>' - The value of the name attribute from the HTML form element.</p>
	<p>Field '<strong>value</strong>' - The value entered in the field. This will vary depending upon the HTML form element.</p>

	<h3>Form Name-Value Pairs</h3>
	<?php

		//This code pulls the field name and value attributes from the Post file
		//It will create a table and display one set of name value pairs per row
			echo "<table border='1'>";
			echo "<tr><th>Field Name</th><th>Value of field</th></tr>";
			foreach($_POST as $key => $value)
			{
				echo '<tr>';
				echo '<td>',$key,'</td>';
				echo '<td>',$value,'</td>';
				echo "</tr>";
			} 
			echo "</table>";
			echo "<p>&nbsp;</p>";

		//This code pulls the field name and value attributes from the Post file
		//It is building a string of data that will become the body of the email

			$body = "Form Data\n\n";			//stores the content of the email
			foreach($_POST as $key => $value)
			{
				$body.= $key."=".$value."\n";
			}
	
			$to = "contactme@dakotareid.com";		//change this to your address
 			$subject = "WDV341 Test Email";		//change this for Subject line
			$headers = "From: null";
 			if (mail($to, $subject, $body, $headers)) 	//puts pieces together and emails
			{
   				echo("<p>Message successfully sent!</p>");
  			} 
			else 
			{
   				echo("<p>Message delivery failed...</p>");
				echo("<p>Still have not figured how to get this to work correctly.</p>");
  			}
	?>

</body>
</html>
