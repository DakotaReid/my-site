<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>WDV341 Intro PHP - Login and Control Page</title>

	<!--  User Login Page
	This page uses the following logic algorithm to do all processing on this page. 

		if user is valid 								"They have already signed into the site"
			then display the admin options
		else
			if the form has been submitted   				"They are in the process of signing into the site"
    			then validate username and password
        			if user is valid							"They entered a valid username and password"
            			set user to valid 
            			return to top of this page
            		else										"The username or password was invalid"
            			display error message
                		display the login form
   			else											"They just got here and want to sign into the site
  				then display login form

	-->
	</head>

<body>
	<?php
		if($_SESSION['validUser'] == "yes") {		//is this already a valid user?
		
	//turn off PHP and turn on HTML
	?>
			<h1>Display Admin Options</h1>
			<p><a href="inputEntry.html">Input New Entry</a></p>
			<p><a href="updateEntry.php">Update Entry</a></p>
			<p><a href="deleteEntry.php">Delete Entry</a></p>
			<p><a href="logout.php">Logout of Admin System</a></p>	
        					
	<?php	//turn off HTML and PHP
		}
		else {
			if(isset($_POST['submitLogin'])) {			//Was this page called from a submitted form?
				$inUsername = $_POST['loginUsername'];	//pull the username from the form
				$inPassword = $_POST['loginPassword'];	//pull the password from the form
			
				include ('dbConnect.php');				//Connect to the database
			
				$sql = "SELECT * FROM event_users WHERE event_username = '" . $inUsername . "' AND event_password = '" . $inPassword . "'";				
				//this SQL command will only work if BOTH the username and password on the table
			
				$result = mysql_query($sql) or die("SQL Error " . mysql_error() . "<p>SQL String: $sql</p>" );
			
				//echo "<h2>SQL: $sql </h2>";			//For Testing purposes
				if(mysql_num_rows($result) == 1) {		//If this is a valid user there should be ONE row only
					$_SESSION['validUser'] = "yes";				//this is a valid user so set your SESSION variable
					//Valid User can do the following things:
	//turn off PHP and begin HTML
	?>
					<h1>Display Admin Options</h1>
       		 		<p><a href="inputEntry.html">Input New Entry</a></p>
        			<p><a href="updateEntry.php">Update Entry</a></p>
        			<p><a href="deleteEntry.php">Delete Entry</a></p>
        			<p><a href="logout.php">Logout of Admin System</a></p>	

	<?php //turn off HTML and turn on PHP								
				}
				else {									//This is an invalid user not in the database
					echo "<h3>Invalid username or password.  Please try again.</h3>";	//sets error message
					//display login form again with the error message.

	//turn off PHP and begin HTML
	?>

					<form method="post" name="login" action="login.php" >
					  <p>Username: <input name="loginUsername" type="text" /></p>
					  <p>Password: <input name="loginPassword" type="password" /></p>
					  <p><input name="submitLogin" value="Login" type="submit" /> <input name="" type="reset" />&nbsp;</p>
					</form>
                
	<?php //turn off HTML and turn on PHP
				}//end of checking for a valid user
			}//end of checking for a submitted page
			else {	//This page was not submitted so the user needs  to se the sign on form to continue
				//display the login form in the area below
			
	//turn off PHP and begin HTML			
	?>
				<h1>Please login to the Admin System</h1>
				<form method="post" name="loginForm" action="login.php">
	  				<p>Username: <input name="loginUsername" type="text" /></p>
	  				<p>Password: <input name="loginPassword" type="password" /></p>
	  				<p><input name="submitLogin" value="Login" type="submit" /> <input name="" type="reset" />&nbsp;</p>
				</form>
    
	<?php //turn off HTML and turn on PHP
			}//ends if statement to check for form submit
		}//end if checking for a valid user

	//turn off PHP and begin HTML
	?>

	<a href="login.php">Return to Login Page</a>
</body>
</html>