<!DOCTYPE html>
<html>
<head>
	<title>WDV341 Into PHP  - Presenters CMS Example</title>

	<?php
		include '../dbConnect.php';

		//Get the name value pairs from the $_POST variable into PHP variables
		//For this example I am using PHP variables with the same name as the name atribute from the HTML form
		$presenter_first_name = $_POST[presenter_first_name];
		$presenter_last_name = $_POST[presenter_last_name];
		$presenter_city = $_POST[presenter_city];
		$presenter_st = $_POST[presenter_st];
		$presenter_zip = $_POST[presenter_zip];
		$presenter_email = $_POST[presenter_email];
		$presenter_id = $_POST[presenter_id];			//from the hidden field of the update form

		/*
			Create - INSERT
			Read - SELECT
			Update - UPDATE (Add + Read)
			Delete - DELETE WHERE

			Update Form:
				-Display the data on the form:
					SELECT:
						Place data in the form fields
						Display the form to user
						User submits the form

				-Update the record in the form:
					UPDATE SET WHERE recID="<x>":
						Overlays the data (Fields that are unchanged in the update form will remain unchanged.)
		*/
	?>
</head>

<body>
	<h1>WDV341 Intro PHP</h1>
	<h2>Presenters CMS Example</h2>
	<h3>UPDATE Record Process</h3>
	<p>This page is called from the presentersUpdateForm.php using the form's action attribute. This page will pull the form data from the $_POST array into PHP variables. </p>
	<p>It will then build the SQL UPDATE query using the PHP variables. The query will overwrite the existing fields in the database with the new values provided by the form. </p>
	<p>If the query processes correctly this page will display a confirmation message to the user/customer. If not, this page will display an error message to the user/customer. </p>
	<p>Note: In a production environment this error message should be user/customer friendly. Additional information should be sent to the developer so that they can see what happened when they attempt to fix it. </p>
	
	<?php
	//Create the SQL UPDATE query or command  
		$sql = "UPDATE wdv341_presenters SET " ;
		$sql .= "presenter_first_name='$presenter_first_name', ";
		$sql .= "presenter_last_name='$presenter_last_name', ";
		$sql .= "presenter_city='$presenter_city', ";
		$sql .= "presenter_st='$presenter_st', ";
		$sql .= "presenter_zip='$presenter_zip', ";		
		$sql .= "presenter_email='$presenter_email' ";		//NOTE last one does NOT have a comma after it
		$sql .= " WHERE (presenter_id='$presenter_id')";		//VERY IMPORTANT  Only delete the requested record id
	
		//echo "<h3>$sql</h3>";			//testing

		if (mysqli_query($link,$sql) )
		{
			echo "<h1>Your record has been successfully UPDATED the database.</h1>";
			echo "<p>Please <a href='../8 - SQL Delete/displayPresenters.php'>view</a> your records.</p>";
		}
		else
		{
			echo "<h1>You have encountered a problem.</h1>";
			echo "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
		}

		mysqli_close($link);	//closes the connection to the database once this page is complete.
	?>
</body>
</html>