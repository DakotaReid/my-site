﻿<!DOCTYPE html>
<html>
<head>
    <title>WDV341 INSERT</title>
</head>

<body>
    <?php
		include "../dbConnect.php";

		/*"SELECT <columnName>, [<columnName>]...
		   FROM <tableName>"	(Result Object, what we process in PHP)*/
		//"WHERE <condition>" (Filter)
		//"ORDER BY <columnName>, [<columnName>]... <ASC|DESC>" (Sort)
		//General Rule: do as much processing in SQL as possible. (Faster and more efficient.)

		$sql = "SELECT * FROM wdv341_presenters";
		$result = mysqli_query($link, $sql);

		if(!$result)
		{
			echo "<h1 style='color: red'>Houston, We have a problem!</h1>";
			echo mysqi_error($link);
		}
	?>

	<h1>WDV341 Intro PHP</h1>
	<h2>Presenters CMS Example</h2>
	<h3>View Presenters</h3>
	<p>This page will pull all of the presenters from the presenters table in the database. It will display all of the columns for each presenter as an HTML table. </p>
	<p>Each presenter has an Update link. The update link will call a form page that uses PHP to fill out the form for the chosen presenter. Notice how the link passes the presenter_id as a GET parameter on the URL in the href attribute of the hyperlink element.</p>
	<p>Each presenter has a Delete link. The delete link will call a php page that will delete the selected record from the table. Notice how the link passes the presenter_id as a GET parameter on the URL in the href attribute of the hyperlink element.</p>

	<?php
		echo "<h3>" . mysqli_num_rows($result). " records were found.</h3>";
	?>
	
	<div>
		<table border="1">
		<tr>
			<th>First Name</th>
			<th>Last Name</th>
			<th>City</th>
			<th>State</th>
			<th>Zip</th>
			<th>Email Address</th>
			<th>Update</th>
			<th>Delete</th>
		</tr>

		<?php
			while($row = mysqli_fetch_array($result))
  			{
  				echo "<tr>";
  				echo "<td>" . $row['presenter_first_name'] . "</td>";
  				echo "<td>" . $row['presenter_last_name'] . "</td>";
  				echo "<td>" . $row['presenter_city'] . "</td>";
  				echo "<td>" . $row['presenter_st'] . "</td>";
  				echo "<td>" . $row['presenter_zip'] . "</td>";
  				echo "<td>" . $row['presenter_email'] . "</td>";
				echo "<td><a href='../9 - SQL Update/presentersUpdateForm.php?recId=" . $row['presenter_id'] . "'>Update</a></td>"; //Processes the "presenter_id' (Uses the "Get" function from the form)
				echo "<td><a href='deletePresenters.php?recId=" . $row['presenter_id'] ."'>Delete</a></td>";
  				echo "</tr>";
  			}

			mysqli_close($link);
		?>
		</table>
	</div>	
</body>
</html>