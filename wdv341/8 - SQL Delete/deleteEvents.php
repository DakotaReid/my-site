<?php
	include "../dbConnect.php";

	$deleteRecId = $_GET['recId'];

	if(isset($_POST['confirmButton'])) {
		$sql = "DELETE FROM wdv341_events WHERE event_id = $deleteRecId";
	
		if (mysqli_query($link, $sql)) {
			echo "Your record has been successfully deleted.";
            //header("Location: http://dakotareid.info/wdv341/7%20-%20SQL%20Select%20and%20Display/selectAndDisplayEvents.php");
			echo "<br /><a href='../7 - SQL Select and Display/selectAndDisplayEvents.php'>View your change.</a>";
		}
		else {
			echo "You have encountered a problem with your delete: " . mysqli_error($link);
		}

		mysqli_close($link);
	}
	else {
?>

<!DOCTYPE html>
<html>
<head>
	<title>WDV341 Delete</title>
</head>

<body>
	<h1>Are you sure that you want to delete the record? (This cannot be undone!)</h1>

	<form style="float: left" name="confimation" method="post" action="deleteEvents.php?recId=<?php echo $deleteRecId; ?>">
		<input type="submit" name="confirmButton" value="Yes" />
	</form>

	<form name="notConfirmed" method="post" action="../7 - SQL Select and Display/selectAndDisplayEvents.php">
		<input type="submit" name="declineButton" value="No"  style="margin-left: 16px" />
	</form>
</body>
</html>

<?php
	}
?>