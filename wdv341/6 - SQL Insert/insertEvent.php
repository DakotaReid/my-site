<!DOCTYPE html>
<html>
<head>
	<title>WDV341 Into PHP  - Presenters CMS Example</title>

	<?php
		include "../dbConnect.php";

		$event_name = $_POST['event_name'];
		$event_description = $_POST['event_description'];
		$event_presenter = $_POST['event_presenter'];
		/*$event_day = $_POST[event_day];
		$event_time = $_POST[event_time];*/

		$sql = "INSERT INTO wdv341_events (";
		$sql .= "event_name, ";
		$sql .= "event_description, ";
		$sql .= "event_presenter ";
		/*$sql .= "event_day, ";
		$sql .= "event_time ";*/

		$sql .= ") VALUES (";
		$sql .= "'$event_name',";
		$sql .= "'$event_description',";
		$sql .= "'$event_presenter'";
		/*$sql .= "'event_day', ";
		$sql .= "'event_time', ";*/
		$sql .= ");";
	?>
</head>

<body>
	<?php
		if(mysqli_query($link, $sql))
		{
			echo "<h1>Your record has been successfully added to the database.</h1>";
			echo "<table border='1'>";
			echo "<tr><th>Field Name</th><th>Value of field</th></tr>";

			foreach($_POST as $key => $value)
			{
				echo '<tr>';
				echo '<td>',$key,'</td>';
				echo '<td>',$value,'</td>';
				echo "</tr>";
			} 
			echo "</table>";
			echo "<p>&nbsp;</p>";
		}
		else
		{
			echo "<h1>You have encountered a problem.</h1>";
			echo "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
		}

		mysqli_close($link);
	?>
</body>
</html>