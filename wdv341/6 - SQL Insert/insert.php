﻿<!DOCTYPE html>
<html>
<head>
    <title>WDV341 INSERT</title>
</head>

<body>
    <?php
		include "../dbConnect.php";

		//"INSERT INTO <tableName> (<columnNames>) VALUES ('<columnValues>')";
		//$sql = "INSERT INTO wdv341_events (event_name) VALUES ('PHP Class')";
		//$sql = "INSERT INTO wdv341_events (event_name) VALUES ('JavaScript Class')";
		$sql = "INSERT INTO wdv341_events (event_name, event_description, event_day, event_time) VALUES ('HTML Class', 'HTML, XHTML, and CSS', '2014-09-18', '13:30')";

		if(mysqli_query($link, $sql))
		{
			echo "<h1>Your record has been successfully added to the database.</h1>";
		}
		else
		{
			echo "<h1>You have encountered a problem.</h1>";
			echo "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
		}

		mysqli_close($link);
	?>
</body>
</html>
