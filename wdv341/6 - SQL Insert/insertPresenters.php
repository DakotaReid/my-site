<!DOCTYPE html>
<html>
<head>
	<title>WDV341 Into PHP  - Presenters CMS Example</title>

	<?php
		include "../dbConnect.php";	//connects to the database

		//Get the name value pairs from the $_POST variable into PHP variables
		//For this example I am using PHP variables with the same name as the name atribute from the HTML form
		$presenter_first_name = $_POST[presenter_first_name];
		$presenter_last_name = $_POST[presenter_last_name];
		$presenter_city = $_POST[presenter_city];
		$presenter_st = $_POST[presenter_st];
		$presenter_zip = $_POST[presenter_zip];
		$presenter_email = $_POST[presenter_email];

		//SQL EXAMPLE #1
		//Build the SQL Command to add the record  This technique is good for adding a lot of fields 
		// or when you need to add and remove fields as your develop/test your application.
		$sql = "INSERT INTO wdv341_presenters (";
		$sql .= "presenter_first_name, ";
		$sql .= "presenter_last_name, ";
		$sql .= "presenter_city, ";
		$sql .= "presenter_st, ";
		$sql .= "presenter_zip, ";
		$sql .= "presenter_email ";		//Last column in the list does NOT have a comma after it.

		$sql .= ") VALUES (";
		$sql .= "'$presenter_first_name',";
		$sql .= "'$presenter_last_name',";
		$sql .= "'$presenter_city',";
		$sql .= "'$presenter_st', ";
		$sql .= "'$presenter_zip', ";
		$sql .= "'$presenter_email' ";	//Last column in the list does NOT have a comma after it.

		$sql .= ");";
		//Test the SQL command to see if it correctly formatted.
		//echo "<p>$sql</p>";
	
		//SQL EXAMPLE #2		
		//Build the SQL Command to add the record  This technique is good when you have a small number of fields
		// or you will not be changing the fields very often, like once it is production.
		//NOTE:  This is one continous command line.  Do not use any returns or this will break.
		$sqlHardCode = "INSERT INTO wdv341_presenters (presenter_first_name, presenter_last_name, presenter_city, presenter_st, presenter_zip, presenter_email) VALUES ($presenter_first_name, $presenter_last_name, $presenter_city, $presenter_st, $presenter_zip, $presenter_email);";
		//Test the SQL command to see if it correctly formatted.
		//echo "<p>$sqlHardCode</p>";	
	
	
		//SQL EXAMPLE #3	
		//Build the SQL Command to add the record.  This technique uses the $_POST variable to place the values
		// in the SQL string.  You have to use concatenation to make this work.  I find this format hard to work with.
		//NOTE:  This is one continous command line.  Do not use any returns or this will break.
		$sqlPost = "INSERT INTO wdv341_presenters (presenter_first_name, presenter_last_name, presenter_city, presenter_st, presenter_zip, presenter_email) VALUES (" . $_POST['presenter_first_name'] . ", " . $_POST['presenter_last_name'] . ", " . $_POST['presenter_city'] . ", " . $_POST['presenter_st'] . ", " . $_POST['presenter_zip'] . ", " . $_POST['presenter_email'] . ");"; 
		//Test the SQL command to see if it correctly formatted.
		//echo "<p>$sqlPost</p>";
	?>
</head>

<body>
	<h1>WDV341 Intro PHP </h1>
	<h2>Presenters CMS Example</h2>
	<h3>Add Record to the Database</h3>
	<p>This page is called by the presentersForm.html when the form is submitted. This page will pull the form data from the $_POST variable into PHP variables.</p>
	<p>It will then build the SQL INSERT command using the variables with data from the from.  If the query runs correctly this page will display a confirmation message to the user/customer. If there are problems with the INSERT then an error message will be displayed.</p>
	<p>Note: In a production environment this error message should be user/customer friendly. Additional information should be sent to the developer so that they can see what happened when they attempt to fix it. </p>
	<p>&nbsp;</p>

	<?php
		//Run the SQL command using the database you connected with
		if(mysqli_query($link, $sql) )
		{
			echo "<h1>Your record has been successfully added to the database.</h1>";
			//echo "<p>Please <a href='viewPresenters.php'>view</a> your records.</p>";

			echo "<table border='1'>";
			echo "<tr><th>Field Name</th><th>Value of field</th></tr>";
			foreach($_POST as $key => $value)
			{
				echo '<tr>';
				echo '<td>',$key,'</td>';
				echo '<td>',$value,'</td>';
				echo "</tr>";
			} 
			echo "</table>";
			echo "<p>&nbsp;</p>";
		}
		else
		{
			echo "<h1>You have encountered a problem.</h1>";
			echo "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
		}

		mysqli_close($link);	//closes the connection to the database once this page is complete.
	?>
</body>
</html>