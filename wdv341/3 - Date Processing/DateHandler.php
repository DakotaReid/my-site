<!DOCTYPE html>
<html>
<head>
	<title>Date Handling</title>
</head>

<body>
	<h1>Project 3 - Date Handling</h1>

	<?php
		$timezone = new DateTimeZone('America/Chicago');
		$date = new DateTime('7/1/2014', $timezone);
		$date2 = new DateTime('9/1/2014', $timezone);
		$currDate = new DateTime(null, $timezone);
		$interval = $date->diff($currDate);
		$interval2 = $date2->diff($currDate);

		echo $date->format('m\/d\/Y') . "<br />";
		echo $date->format('d\/m\/Y') . "<br />";
		echo $date->format('l F n \, Y') . "<br />";

		echo "The number of days between " . $date->format('n\/j\/Y') . " and " . $currDate->format('n\/j\/Y') . " is: " . $interval->format('%a days') . "<br />";

		echo "The number of hours between " . $date2->format('n\/j\/Y') . " and " . $currDate->format('n\/j\/Y') . " is: " . $interval2->format('%h hours') . "<br />";
	?>

</body>
</html>