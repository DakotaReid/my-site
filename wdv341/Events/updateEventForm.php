<!DOCTYPE html>
<html>
<head>
	<title>Update Event Form</title>
	<link rel="stylesheet" href="../../style.css" />

	<?php
		include ("../dbConnect.php");

		$updateRecId = $_GET['recId'];
		$sql = "SELECT * FROM wdv341_events WHERE event_id = $updateRecId";
		$result = mysqli_query($link,$sql);

		if(!$result) {
			echo "<h1>You have encountered a problem with your update.</h1>";
			die( "<h2>" . mysqli_error($link) . "</h2>") ;
		}

		$row = mysqli_fetch_array($result);
	?>
</head>

<body>
	<div id="wrapper">
		<div id="cssmenu">
			<ul>
				<li><a href="index.html">Dakota Reid</a></li>
				<li>
					<a href="hobbies.html">Hobbies</a>
					<ul>
						<li><a href="hobbies.html#photography">Photography</a></li>
					</ul>
				</li>
				<li>
					<a href="projects.html">Projects</a>
					<ul>
						<li><a href="projects.html#cpp">C++</a></li>
						<li><a href="projects.html#java">Java</a></li>
						<li><a href="projects.html#javascript">JavaScript</a></li>
					</ul>
				</li>
				<li>
					<a href="webdev.html" class="active">Web Development</a>
					<ul>
						<li><a href="wdv101.html">WDV101</a></li>
						<li><a href="wdv341.html">WDV341</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div id="content">
			<h1>Update Event Information</h1>

			<form name="eventForm" method="post" action="updateEvents.php?recId=<?php echo $updateRecId; ?>">
				<p>
					Event Name:
					<input type="text" name="event_name" value="<?php echo $row['event_name']; ?>" />
				</p>

				<p>
					Event Description:
					<input type="text" name="event_description" value="<?php echo $row['event_description']; ?>" />
				</p>

				<p>
					Event Presenter:
					<input type="text" name="event_presenter" value="<?php echo $row['event_presenter']; ?>" />
				</p>

				<p>
					Event Day:
					<input type="text" name="event_date" value="<?php echo $row['event_date']; ?>" />
				</p>

				<p>
					Event Time:
					<input type="text" name="event_time" value="<?php echo $row['event_time']; ?>" />
				</p>

				<input type="submit" name="updateEvent" value="Update Event" />
				<input type="reset" name="clearForm" value="Clear Form" />
			</form>
		</div>
	</div>
</body>
</html>
