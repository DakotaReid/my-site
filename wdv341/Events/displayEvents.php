﻿<?php session_start(); ?>

<?php
	if($_SESSION['validUser'] != "yes") {
		header("Location: ../10 - Login/login.php");
	}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Display Events Page</title>
	<link rel="icon" href="../../images/table.ico" />
	<link rel="stylesheet" href="../../style.css" />
	<style>
		#content a:link,
		#content a:active,
		#content a:visited {
			color: #ccc;
		}

		#content a:hover {
			text-decoration: none;
		}
	</style>
</head>

<body>
    <div id="wrapper">
		<div id="cssmenu">
			<ul>
				<li><a href="../10 - Login/login.php">Admin Options</a></li>
				<li><a href="insertEvent.php">Insert</a></li>
				<li><a href="displayEvents.php" class="active">Display/Modify</a></li>
				<li style="float: right;"><a href="../10 - Login/logout.php" style="margin-right: 0;">Logout</a></li>
			</ul>
		</div>

		<div id="content">
			<h1>Events</h1>

			<?php include ("displayEventTable.php"); ?>

			<ul><li><a href="../10 - Login/login.php">Return to Admin Options</a></li></ul>
		</div>
	</div>
</body>
</html>