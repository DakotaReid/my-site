<?php session_start(); ?>

<?php
	if($_SESSION['validUser'] != "yes") {
		header("Location: ../10 - Login/login.php");
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Update Event Page</title>
	<link rel="icon" href="../../images/form_edit.ico" />
	<link rel="stylesheet" href="../../style.css" />

	<style>
		form {
			width: 35.2%;
		}

		label {
			width: 10em;
			margin-right: 1em;
			text-align: right;
			float: left;
		}

		.textbox {
			display: block;
		}
	</style>

	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<script>
		$.datepicker.setDefaults({
			dateFormat: "yy-mm-dd"
		});
		$(function() {
			$( "#datepicker" ).datepicker({
				showButtonPanel: true
			});
		});
	</script>

	<?php
		include ("../dbConnect.php");

		$event_id = $_GET['recId'];
		$event_name = $_POST[event_name];
		$event_description = $_POST[event_description];
		$event_presenter = $_POST[event_presenter];
		$event_date = $_POST[event_date];
		$event_time = $_POST[event_time];

		$updateRecId = $_GET['recId'];
		$sql = "SELECT * FROM wdv341_events WHERE event_id = $updateRecId";
		$result = mysqli_query($link,$sql);

		if(!$result) {
			echo "<h1>You have encountered a problem with your update.</h1>";
			die( "<h2>" . mysqli_error($link) . "</h2>") ;
		}

		$row = mysqli_fetch_array($result);
	?>
</head>

<body>
	<div id="wrapper">
		<div id="cssmenu">
			<ul>
				<li><a href="../10 - Login/login.php">Admin Options</a></li>
				<li><a href="insertEvent.php">Insert</a></li>
				<li><a href="displayEvents.php" class="active">Display/Modify</a></li>
				<li style="float: right;"><a href="../10 - Login/logout.php" style="margin-right: 0;">Logout</a></li>
			</ul>
		</div>

		<div id="content">
		<?php
			if(isset($_POST['updateEvent'])) {
				$sql = "UPDATE wdv341_events SET " ;
				$sql .= "event_name='$event_name', ";
				$sql .= "event_description='$event_description', ";
				$sql .= "event_presenter='$event_presenter', ";
				$sql .= "event_date='$event_date', ";
				$sql .= "event_time='$event_time' ";
				$sql .= " WHERE (event_id='$event_id')";
	
				if(mysqli_query($link, $sql)) {
					echo "<h1>Your record has been successfully updated.</h1>";
			
					include("displayEventTable.php");
				}
				else {
					echo "<h2>You have encountered a problem.</h2>";
					echo "<h3 style='color: red'>" . mysqli_error($link) . "</h3>";
				}

				mysqli_close($link);
			}
			else {
		?>
				<h1>Update Event Information</h1>

				<!--<form name="eventForm" method="post" action="updateEvents.php?recId=<?php echo $updateRecId; ?>">
					<p>
						Event Name:
						<input type="text" name="event_name" value="<?php echo $row['event_name']; ?>" />
					</p>

					<p>
						Event Description:
						<input type="text" name="event_description" value="<?php echo $row['event_description']; ?>" />
					</p>

					<p>
						Event Presenter:
						<input type="text" name="event_presenter" value="<?php echo $row['event_presenter']; ?>" />
					</p>

					<p>
						Event Date:
						<input type="text" id="datepicker" name="event_date" value="<?php echo $row['event_date']; ?>" />
					</p>

					<p>
						Event Time:
						<input type="text" name="event_time" value="<?php echo $row['event_time']; ?>" />
					</p>

					<input type="submit" name="updateEvent" value="Update Event" />
					<input type="reset" name="clearForm" value="Clear Form" />
				</form>-->

				<form name="eventForm" method="post" action="updateEvents.php?recId=<?php echo $updateRecId; ?>">
					<label for="event_name">Event Name:</label>
					<input type="text" name="event_name" value="<?php echo $row['event_name']; ?>" required="required" />

					<label for="event_description">Event Description:</label>
					<input type="text" name="event_description" value="<?php echo $row['event_description']; ?>" />

					<label for="event_presenter">Event Presenter:</label>
					<input type="text" name="event_presenter" value="<?php echo $row['event_presenter']; ?>" />

					<label for="event_date">Event Date:</label>
					<input type="text" id="datepicker" name="event_date" value="<?php echo $row['event_date']; ?>" />

					<label for="event_time">Event Time:</label>
					<input type="text" name="event_time" value="<?php echo $row['event_time']; ?>" />

					<input type="submit" name="updateEvent" value="Update Event" style="float: right;" />
					<input type="reset" name="clearForm" value="Clear Form" style="margin-right: 1em; float: right;" />
				</form>
		<?php
			}
		?>
			<ul style="clear: both;"><li><a href="displayEvents.php">Return to Display/Modify Page</a></li></ul>
		</div>
	</div>
</body>
</html>