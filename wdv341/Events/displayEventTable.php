<?php session_start(); ?>

<?php
	if($_SESSION['validUser'] != "yes") {
		header("Location: ../10 - Login/login.php");
	}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Events Table</title>
	<link rel="stylesheet" href="../../style.css" />
	<style>
		table a:link,
		table a:active,
		table a:visited {
			color: #ccc;
		}

		table a:hover {
			text-decoration: none;
		}
	</style>
</head>

<body>
    <?php
		include ("../dbConnect.php");

		$sql = "SELECT * FROM wdv341_events";
		$result = mysqli_query($link, $sql);

		if(!$result) {
			echo mysqi_error($link);
		}
	?>

	<table>
		<tr>
			<th>Event Name</th>
			<th>Description</th>
			<th>Presenter</th>
			<th>Date</th>
			<th>Time</th>
			<th>Delete</th>
			<th>Update</th>
		</tr>

	<?php
		while($row = mysqli_fetch_array($result)) {
  			echo "<tr>";
  			echo "<td>" . $row['event_name'] . "</td>";
  			echo "<td>" . $row['event_description'] . "</td>";
  			echo "<td>" . $row['event_presenter'] . "</td>";
  			echo "<td>" . $row['event_date'] . "</td>";
  			echo "<td>" . $row['event_time'] . "</td>";
			echo "<td><a href='deleteEvents.php?recId=" . $row['event_id'] ."'>Delete</a></td>";
			echo "<td><a href='updateEvents.php?recId=" . $row['event_id'] ."'>Update</a></td>";
  			echo "</tr>";
  		}

		mysqli_close($link);
	?>
	</table>
</body>
</html>