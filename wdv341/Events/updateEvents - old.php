<!DOCTYPE html>
<html>
<head>
	<title>Update Event</title>
	<link rel="stylesheet" href="../../style.css" />

	<?php
		include ("../dbConnect.php");

		$event_id = $_GET['recId'];
		$event_name = $_POST[event_name];
		$event_description = $_POST[event_description];
		$event_presenter = $_POST[event_presenter];
		$event_date = $_POST[event_date];
		$event_time = $_POST[event_time];
	?>
</head>

<body>
	<div id="wrapper">
		<div id="cssmenu">
			<ul>
				<li><a href="index.html">Dakota Reid</a></li>
				<li>
					<a href="hobbies.html">Hobbies</a>
					<ul>
						<li><a href="hobbies.html#photography">Photography</a></li>
					</ul>
				</li>
				<li>
					<a href="projects.html">Projects</a>
					<ul>
						<li><a href="projects.html#cpp">C++</a></li>
						<li><a href="projects.html#java">Java</a></li>
						<li><a href="projects.html#javascript">JavaScript</a></li>
					</ul>
				</li>
				<li>
					<a href="webdev.html" class="active">Web Development</a>
					<ul>
						<li><a href="wdv101.html">WDV101</a></li>
						<li><a href="wdv341.html">WDV341</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div id="content">
		<?php
			$sql = "UPDATE wdv341_events SET " ;
			$sql .= "event_name='$event_name', ";
			$sql .= "event_description='$event_description', ";
			$sql .= "event_presenter='$event_presenter', ";
			$sql .= "event_date='$event_date', ";
			$sql .= "event_time='$event_time' ";
			$sql .= " WHERE (event_id='$event_id')";

			if(mysqli_query($link, $sql)) {
				echo "<h1>Your record has been successfully UPDATED the database.</h1>";
				echo "<p>Please <a href='selectAndDisplayEvents.php'>view</a> your records.</p>";
			}
			else {
				echo mysqli_error($link);
			}

			mysqli_close($link);
		?>
		</div>
	</div>
</body>
</html>