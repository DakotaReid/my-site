<!DOCTYPE html>
<html>
<head>
	<title>WDV341 Insert Event</title>
	<link rel="stylesheet" href="../../style.css" />
	<style>
		#content a:link,
		#content a:active,
		#content a:visited {
			color: #ccc;
		}

		#content a:hover {
			text-decoration: none;
		}
	</style>

	<?php
		include ("../dbConnect.php");

		$event_name = $_POST['event_name'];
		$event_description = $_POST['event_description'];
		$event_presenter = $_POST['event_presenter'];
		$event_date = $_POST[event_date];
		$event_time = $_POST[event_time];

		$sql = "INSERT INTO wdv341_events (";
		$sql .= "event_name, ";
		$sql .= "event_description, ";
		$sql .= "event_presenter, ";
		$sql .= "event_date, ";
		$sql .= "event_time ";

		$sql .= ") VALUES (";
		$sql .= "'$event_name',";
		$sql .= "'$event_description',";
		$sql .= "'$event_presenter',";
		$sql .= "'$event_date',";
		$sql .= "'$event_time'";
		$sql .= ");";
	?>
</head>

<body>
	<div id="wrapper">
		<div id="cssmenu">
			<ul>
				<li><a href="index.html">Dakota Reid</a></li>
				<li>
					<a href="hobbies.html">Hobbies</a>
					<ul>
						<li><a href="hobbies.html#photography">Photography</a></li>
					</ul>
				</li>
				<li>
					<a href="projects.html">Projects</a>
					<ul>
						<li><a href="projects.html#cpp">C++</a></li>
						<li><a href="projects.html#java">Java</a></li>
						<li><a href="projects.html#javascript">JavaScript</a></li>
					</ul>
				</li>
				<li>
					<a href="webdev.html" class="active">Web Development</a>
					<ul>
						<li><a href="wdv101.html">WDV101</a></li>
						<li><a href="wdv341.html">WDV341</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div id="content">
		<?php
			if(mysqli_query($link, $sql)) {
				echo "<h2>Your record has been successfully added to the database.</h2>";

				include ("displayEventTable.php");
			}
			else {
				echo "<h2>You have encountered a problem.</h2>";
				echo "<h3 style='color: red'>" . mysqli_error($link) . "</h3>";
			}

			mysqli_close($link);
		?>
			<ul><li><a href="../10 - Login/login.php">Return to Login Page</a></li></ul>
		</div>
	</div>
</body>
</html>