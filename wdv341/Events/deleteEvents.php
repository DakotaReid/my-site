<?php session_start(); ?>

<?php
	if($_SESSION['validUser'] != "yes") {
		header("Location: ../10 - Login/login.php");
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Delete Event Page</title>
	<link rel="icon" href="../../images/table_delete.ico" />
	<link rel="stylesheet" href="../../style.css" />
</head>

<body>
	<div id="wrapper">
		<div id="cssmenu">
			<ul>
				<li><a href="../10 - Login/login.php">Admin Options</a></li>
				<li><a href="insertEvent.php">Insert</a></li>
				<li><a href="displayEvents.php" class="active">Display/Modify</a></li>
				<li style="float: right;"><a href="../10 - Login/logout.php" style="margin-right: 0;">Logout</a></li>
			</ul>
		</div>

		<div id="content">
		<?php
			include ("../dbConnect.php");

			$deleteRecId = $_GET['recId'];

			if(isset($_POST['confirmButton'])) {
				$sql = "DELETE FROM wdv341_events WHERE event_id = $deleteRecId";
	
				if(mysqli_query($link, $sql)) {
					echo "<h1>Your record has been successfully deleted.</h1>";
			
					include("displayEventTable.php");
				}
				else {
					echo "<h2>You have encountered a problem.</h2>";
					echo "<h3 style='color: red'>" . mysqli_error($link) . "</h3>";
				}

				mysqli_close($link);
			}
			else {
		?>
					<h1 style="margin-bottom: 0;">Are you sure that you want to delete the record?</h1>
					<h2 style="margin-top: 0;">(This cannot be undone!)</h2>

					<form style="float: left" name="confimation" method="post" action="deleteEvents.php?recId=<?php echo $deleteRecId; ?>">
						<input type="submit" name="confirmButton" value="Yes" />
					</form>

					<form name="notConfirmed" method="post" action="displayEvents.php">
						<input type="submit" name="declineButton" value="No"  style="margin-left: 1em" />
					</form>
		<?php
			}
		?>
			<ul><li><a href="displayEvents.php">Return to Display/Modify Page</a></li></ul>
		</div>
	</div>
</body>
</html>