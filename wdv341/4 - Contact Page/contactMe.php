<!DOCTYPE html>
<html>
<head>
	<title>Contact Me</title>
	<link rel="stylesheet" href="../../css/style.css" />
</head>

<body>
	<div id="wrapper">
        <div id="cssmenu">
            <ul>
                <li><a href="index.html">Dakota Reid</a></li>
                <li>
                    <a href="hobbies.html">Hobbies</a>
                    <ul>
                        <li><a href="hobbies.html#photography">Photography</a></li>
                    </ul>
                </li>
                <li>
                    <a href="projects.html">Projects</a>
                    <ul>
                        <li><a href="projects.html#cpp">C++</a></li>
                        <li><a href="projects.html#java">Java</a></li>
                        <li><a href="projects.html#javascript">JavaScript</a></li>
                    </ul>
                </li>
                <li>
                    <a href="webdev.html" class="active">Web Development</a>
                    <ul>
                        <li><a href="wdv101.html">WDV101</a></li>
                        <li><a href="wdv341.html">WDV341</a></li>
                    </ul>
                </li>
            </ul>
        </div>

		<div id="content">
			<?php
				$timezone = new DateTimeZone('America/Chicago');
				$date = new DateTime(null, $timezone);

				$to = $_POST['email'];
 				$subject = "Thank You for Contacting Me!";
				$body = "Thank you " . $_POST['firstName'] . " " . $_POST['lastName'] . " for contacting me about " . $_POST['reason'] . ".";
				$headers = "From: contactme@dakotareid.com";

				echo "<h2>" . $body . "</h2>";
				echo $date->format('m\/d\/Y g:i A') . "<br />";

				mail($to, $subject, $body, $headers);
			?>

			<?php
				$timezone = new DateTimeZone('America/Chicago');
				$date = new DateTime(null, $timezone);

				$to = "contactme@dakotareid.com";
				$subject = $_POST['reason'];
				$body = $_POST['firstName'] . " " . $_POST['lastName'] . " Contacted You About " . $_POST['reason'] . " at " . $date->format('g:i A') . " on " . $date->format('m\/d\/Y') . ".<br /><br />" . $_POST['comments'] . "<br />";
				$headers = "From: " . $_POST['email'];

				echo "<br />";
				echo "This is what the body of the email that would be sent to the webmaster email would look like:";
				echo "<br />";
				echo $body;

				mail($to, $subject, $body, $headers);
			?>
		</div>

		<footer>
            <p>Dakota Reid</p>
        </footer>
	</div>
</body>
</html>