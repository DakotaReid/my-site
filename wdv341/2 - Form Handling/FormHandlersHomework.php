﻿<!DOCTYPE html>
<html>
<head>
    <title>WDV341 Intro PHP - Project-6 Form Handlers Homework</title>
</head>

<body>
       
	<?php
		$body = "Form Data\n\n";
		foreach($_POST as $key => $value)
		{
			$body .= $key."=".$value."\n";
		}
	
		$to = $_POST['cusEmail']; //Looks only for the object with the "cusEmail" class.
 		$subject = "Thank You for Registering!";
		$headers = "From: contactme@dakotareid.com";
 		if (mail($to, $subject, $body, $headers))
		{
			echo "<h1>Thank You for Registering!</h1>";
  		} 
		else 
		{
   			echo "<h1>Message delivery failed...</h1>
				  <p>This is because I don't have an email server on my web server.</p>";
  		}

		echo "<table border='1'>";
		echo "<tr><th>Field Name</th><th>Value of field</th></tr>";
		foreach($_POST as $key => $value)
		{
			echo '<tr>';
			echo	'<td>',$key,'</td>';
			echo	'<td>',$value,'</td>';
			echo "</tr>";
		}
		echo "</table>";
	?>

</body>
</html>
