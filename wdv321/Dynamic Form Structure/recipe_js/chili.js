﻿var chili_serves_og = 6;
var chili_serves = 6;

var chili_ingrs = [
            "Cooking Oil",
            "Onion",
            "Chopped Peppers",
            "Chili Powder",
            "Hot Chili Powder (optional)",
            "Ground Beef or Chicken",
            "Red Beans",
            "Kidney Beans",
            "Tomato Puree",
            "Tomato Sause",
            "Shredded Cheese (optional)",
            "Sour Cream (optional)"
];

var chili_units = [
    "tbsp.",
    "cup(s)",
    "cup(s)",
    "tbsp.",
    "tsp.",
    "lb(s)",
    "can(s)",
    "can(s)",
    "can(s)",
    "can(s)",
    "cup(s)",
    "cup(s)"
];

var chili_amounts_og = [
    2,
    1,
    1,
    4,
    1,
    1,
    2,
    2,
    2,
    2,
    1,
    1 / 2
];

var chili_amounts = [
    2,
    1,
    1,
    4,
    1,
    1,
    2,
    2,
    2,
    2,
    1,
    1 / 2
];

function displayChili() {
    $.each(chili_ingrs, function (i, value) {
        $("#chili").append("<p>" + chili_amounts[i] + " " + chili_units[i] + " " + value + "</p>");
    });
}

function doubleChili() {
    $.each(chili_amounts, function (i, value) {
        chili_amounts[i] = value * 2;
    });
}

function halveChili() {
    $.each(chili_amounts, function (i, value) {
        chili_amounts[i] = value / 2;
    });
}

function resetChili() {
    $.each(chili_amounts, function (i, value) {
        chili_amounts[i] = chili_amounts_og[i];
    });
}