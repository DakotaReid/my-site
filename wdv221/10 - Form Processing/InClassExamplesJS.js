﻿//This will display the value of the text field
function displayText() {
    var text = document.getElementById("textField");

    if (text.value != "") {
        alert(text.value);
    }
}

//This will set the value of the text field to "Please enter a value"
function setText() {
    var text = document.getElementById("textField");

    //text.value = "Please enter a value";

    //Used this because it is easier to enter values
    text.value = "";
    text.placeholder = "Please enter a value";
}

//This will display the value of the text area
function displayTextAreaText() {
    var textArea = document.getElementById("textArea");

    if (textArea.value != "") {
        alert(textArea.value);
    }
}

//This will set the value of the text area to "This is where I type my comments."
function setTextAreaText() {
    var textArea = document.getElementById("textArea");

    //textArea.value = "This is where I type my comments.";

    textArea.value = "";
    textArea.placeholder = "This is where I type my comments.";
}

/*
 *   This will display:
 *   - The checked status of all the buttons
 *   - The value of the selected radio button
 */
function displayRadioButtons() {
    var radioButtons = document.getElementsByName("radioButtons");
    var msg = "";
    var val = "";

    for (i = 0; i < radioButtons.length; i++) {
        if (radioButtons[i].checked) {
            val = radioButtons[i].value;
        }

        msg += "Radio button " + (i + 1) + " is checked: " + radioButtons[i].checked + "\n";
    }

    if (val != "") {
        msg += "\nThe value of the selected radio button: " + val;
    }

    alert(msg);
}

//This will set the middle button to checked
function setRadioButton(value) {
    var radioButtons = document.getElementsByName("radioButtons");

    radioButtons[value].checked = true;
}

/*
 *   This will display:
 *   - The checked status of all the checkboxes
 *   - The value of the selected checkboxes
 */
function displayCheckbox() {
    var checkboxes = document.getElementsByName("checkboxes");
    var msg = "";
    var val = "";

    for (i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
            val += checkboxes[i].value + "\n";
        }

        msg += "Checkbox " + (i + 1) + " is checked: " + checkboxes[i].checked + "\n";
    }

    if (val != "") {
        msg += "\nThe value of the selected checkbox(es): \n" + val;
    }

    alert(msg);
}

//This will set both checkboxes to checked
function setCheckbox(value) {
    var checkboxes = document.getElementsByName("checkboxes");

    for (i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = false;
    }

    checkboxes[value].checked = true;
    checkboxes[value+1].checked = true;
}

/*
 *   This will display:
 *   - The index of the selected option
 *   - The value of the selected option
 *   - The text of the selected option
 */
function displayDropDownList() {
    var list = document.getElementById("dropDown");

    alert("The index of the selected option is: " + list.selectedIndex + "\n"
        + "The value of the selected option is: " + list.value + "\n"
        + "The text of the selected option is: " + list.options[list.selectedIndex].text);
}

//This will set the middle option as the selected option
function setDropDownOption(value) {
    var list = document.getElementById("dropDown");

    list.options[value].text = value + 1;
}

//This will display the value of the file field
function displayFile() {
    var file = document.getElementById("fileInput");

    if (file.value != "") {
        alert(file.value);
    }
}

//This will load the file name pic1.jpg into the field
//There is no easy way to actually do this...
function setFile() {
    //var file = document.getElementById("fileInput");

    //file.value = "pic1.jpg";

    alert("There is no easy way to actually do this...");
}

//This will display the value of the hidden field
function displayHiddenInput() {
    var hidden = document.getElementById("hiddenInput");

    if (hidden.value != "") {
        alert(hidden.value);
    }
}

//This will set the value of the hidden field to "Can't see me!!"
function setHiddenInput() {
    var hidden = document.getElementById("hiddenInput");

    hidden.value = "Can't see me!!";
}