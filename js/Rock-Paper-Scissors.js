function setPlayerRock() {
    var player = "Rock";
    RockPaperScissors(player);
}

function setPlayerPaper() {
    var player = "Paper";
    RockPaperScissors(player);
}

function setPlayerScissors() {
    var player = "Scissors";
    RockPaperScissors(player);
}

function RockPaperScissors(player) {
    var computer = Math.round(Math.random() * 2);

    switch (computer) {
        case 0: computer = "Rock";
            break;
        case 1: computer = "Paper";
            break;
        case 2: computer = "Scissors";
            break;
    }

    document.getElementById("output1").style.visibility="visible";
    document.getElementById("output2").style.visibility="visible";
    document.getElementById("output3").style.visibility="visible";

    if (player === computer) {
        document.getElementById("output1").innerHTML = "You tied.";
    }
    else if ((player === "Rock" && computer === "Scissors") || (player === "Paper" && computer === "Rock") || (player === "Scissors" && computer === "Paper")) {
        document.getElementById("output1").innerHTML = "You win!";
    }
    else {
        document.getElementById("output1").innerHTML = "You lost.";
    }

    document.getElementById("output2").innerHTML = "The computer chose: " + computer;
    document.getElementById("output3").innerHTML = "You chose: " + player;
}