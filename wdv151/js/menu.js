﻿function toggle() {
    var ele = document.getElementById("menu");
    var main = document.getElementsByTagName("main")[0];

    if (ele.style.display == "block") {
        ele.style.display = "none";
        main.style.opacity = "1";
    }
    else {
        ele.style.display = "block";
        main.style.opacity = ".5";
    }
}