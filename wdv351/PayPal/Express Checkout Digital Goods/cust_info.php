<?
	$hostname = "localhost";
	$username = "Dakota";
	$password = "vSVPJ.45jd";
	$database = "paypal";
	$link = mysqli_connect($hostname, $username, $password, $database);

	if(!$link) {
		die("Connect Error (' . mysqli_connect_errno() . ') " . mysqli_connect_error());		
	}
	
	$sql = "SELECT * FROM customer_info";
	$result = mysqli_query($link, $sql);
	
	if(!$result) {
		die(mysqi_error($link));
	}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Customer Info</title>
</head>

<body>
	<table border="1">
    	<tr>
        	<th>ID</th>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Email</th>
			<th>Item Name</th>
			<th>Timestamp</th>
		</tr>
		<? while($row = mysqli_fetch_array($result)) { ?>
            <tr>
            	<td><?= $row["cust_id"]; ?></td>
                <td><?= $row["cust_first_name"]; ?></td>
                <td><?= $row["cust_last_name"]; ?></td>
                <td><?= $row["cust_email"]; ?></td>
                <td><?= $row["item_name"]; ?></td>
                <td><?= date("m/d/Y", strtotime($row["timestamp"])); ?></td>
            </tr>
        <? } ?>
	</table>
</body>
</html>

<? mysqli_close($link); ?>