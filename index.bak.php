<!DOCTYPE html>
<html>
<head>
    <title>Dakota Reid</title>
    <meta charset="UTF-8" />
    <meta name="description" content="My name is Dakota Reid." />
    <meta name="author" content="Dakota Reid" />
    <link rel="icon" href="images/crest.png" />
    <link rel="stylesheet" href="css/style.css" />
    <!--<link rel="stylesheet" href="mobile_style.css" media="handheld" />-->
    <!--<meta name="viewport" content="width=device-width, initial-scale=.75">-->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-49205781-1', 'iastate.edu');
        ga('send', 'pageview');

    </script>

    <script>
        function swapStyleSheet(sheet) {
            var stylesheet = document.getElementsByTagName("link").item(1);

            if (sheet != stylesheet.getAttribute("href")) {
                stylesheet.setAttribute("href", sheet);
            }
        }
    </script>

    <a href="https://plus.google.com/u/0/+DakotaReid/?rel=author"></a>
</head>

<body>
    <? include_once("nav.php"); ?>

    <main>
        <h2>Welcome to my website!</h2>

        <div id="columns">
            <img src="pictures/me.jpg" width="480" height="320" />
            <p>My name is Dakota Reid.</p>
        </div>
    </main>

    <footer class="smallerPage">
        <p>Dakota Reid | <a href="files/DummyDoc.pdf" target="_blank">Resume</a></p>
        <a href="mailto:contactme@dakotareid.com" target="_blank"><img src="images/email.png" id="footerImage" onmouseover="this.src='images/emailInvert.png'" onmouseout="this.src='images/email.png'" /></a>
        <p id="changeCSS"><a onclick="swapStyleSheet('css/style.css')">Desktop</a> | <a onclick="swapStyleSheet('css/mobilestyle.css')">Mobile</a></p>
    </footer>
</body>
</html>